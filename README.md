# Uhura Extension

This is the source code for the extension of the Uhura System (BA Thesis). 

The program can be compiled and executed with maven:

1. add DLVWrapper 3rd party JAR (see https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html)
2. change path to dlv executable (in DLVProgramExecutor.java line 17)

**Maven command to add 3rd party JAR**
```
mvn install:install-file -Dfile=dependencies\DLVWrapper-v4.2.jar -DgroupId=com.dlvsystem 
-DartifactId=dlvwrapper -Dversion=4.2 -Dpackaging=jar
```