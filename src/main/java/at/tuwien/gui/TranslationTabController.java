package at.tuwien.gui;

import at.tuwien.ASPtoNL.ASPtoNLGenerator;
import at.tuwien.ASPtoNL.exceptions.MissingTemplateException;
import at.tuwien.dlv.DLVException;
import at.tuwien.service.IMainGuiService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Created by tobiaskain on 27/04/2017.
 */
public class TranslationTabController implements Initializable{

    @FXML
    public TextArea taError;
    @FXML
    public TextFlow taModels;
    @FXML
    public TextField tfFilter;
    @FXML
    public Button btnSolve;
    @FXML
    public Button btnTranslate;
    @FXML
    public TabPane infoTabs;
    @FXML
    public WebView wvSentencePatterns;
    @FXML
    public StackPane spCNL;
    @FXML
    public StackPane spASP;

    @FXML
    public Label lbModelCnt;
    @FXML
    public Spinner<Integer> spModels;
    @FXML
    public TextFlow taCNLOut;
    @FXML
    public CheckBox cbDiffHighlighting;

    private Text textModels = new Text();
    private Text textCNLOut = new Text();

    public CodeArea caCNL;
    public CodeArea caASP;

    private Tab tab;
    private File file;
    private String initialCNLContent ="";
    private String tabLabel;

    private ASPtoNLGenerator generator;
    private List<String> models;
    private int currentModel = -1;

    private IMainGuiService mainGuiService;
    private TranslationTabController translationTabController;

    private static final int INDEX_NOT_FOUND = -1;

    public TranslationTabController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        translationTabController = this;
        caCNL = new CodeArea();
        caCNL.setParagraphGraphicFactory(LineNumberFactory.get(caCNL));
        caCNL.setOnKeyReleased(this::tfCnlOnKeyReleased);

        ContextMenu contextMenu = new ContextMenu();
        MenuItem miTranslate = new MenuItem("manually translate sentence");
        miTranslate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String sentence = caCNL.getSelectedText();
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/add_manual_translation.fxml"));

                    Stage stage = new Stage();

                    /* block parent window */
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.initOwner(btnSolve.getScene().getWindow());

                    /* set the scene */
                    stage.setScene(new Scene(loader.load(), 411, 206));

                    stage.setTitle("Add Manual Translation");

                    AddManualTranslationController addManualTranslationController = (AddManualTranslationController) loader.getController();
                    addManualTranslationController.tfCNlSentence.setText(sentence);
                    addManualTranslationController.setMainGuiService(mainGuiService);
                    addManualTranslationController.setTranslationTabController(translationTabController);

                    stage.show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        contextMenu.getItems().add(miTranslate);
        caCNL.setContextMenu(contextMenu);

        spCNL.getChildren().add(0,new VirtualizedScrollPane(caCNL));

        caASP = new CodeArea();
        caASP.setParagraphGraphicFactory(LineNumberFactory.get(caASP));
        spASP.getChildren().add(0, new VirtualizedScrollPane(caASP));

        try {
            loadSentencePatterns();
        } catch (IOException e) {
            e.printStackTrace();
        }

        taModels.getChildren().add(textModels);
        taCNLOut.getChildren().add(textCNLOut);
    }

    public void btnTranslateClicked(ActionEvent actionEvent) {
        translate();
    }

    private void setCurrentModel(int i) {
        this.currentModel = i;
    }

    public void btnSolveClicked(ActionEvent actionEvent) {
        textCNLOut.setText("");
        textModels.setText("");

        List<String> models = null;
        try {
            models = mainGuiService.solve(caASP.getText(),tfFilter.getText());
            this.models = models;
            generator = new ASPtoNLGenerator(caCNL.getText(), caASP.getText(), null);

            lbModelCnt.setText(Integer.toString(models.size()));

            if (models.size() > 0) {
                List<String> finalModels = models;

                SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory<Integer>() {

                    @Override
                    public void decrement(int i) {
                        i = this.getValue();
                        i -= 1;
                        int old = i;
                        i = ((i-1) % finalModels.size());
                        if (i < 0) i += finalModels.size();
                        taModels.getChildren().clear();
                        taModels.getChildren().addAll(formattedAnswerSet(new LinkedList<>(Arrays.asList(finalModels.get(old).split("\n"))), new LinkedList<>(Arrays.asList(finalModels.get(i).split("\n"))), cbDiffHighlighting.isSelected()));
                        taCNLOut.getChildren().clear();
                        taCNLOut.getChildren().addAll(getTranslation(i, old));
                        this.setValue(++i);
                        setCurrentModel(i);
                    }

                    @Override
                    public void increment(int i) {
                        i = this.getValue();
                        i -= 1;
                        int old = i;
                        i = ((i+1) % finalModels.size());
                        taModels.getChildren().clear();
                        taModels.getChildren().addAll(formattedAnswerSet(new LinkedList<>(Arrays.asList(finalModels.get(old).split("\n"))), new LinkedList<>(Arrays.asList(finalModels.get(i).split("\n"))), cbDiffHighlighting.isSelected()));
                        taCNLOut.getChildren().clear();
                        taCNLOut.getChildren().addAll(getTranslation(i, old));
                        this.setValue(++i);
                        setCurrentModel(i);
                    }

                    private List<Text> getTranslation(int idx, int oldidx) {
                        List<Text> ret = new LinkedList<>();
                        try {
                            List<Text> h = formattedText(generator.translate_List(finalModels.get(oldidx)), generator.translate_List(finalModels.get(idx)), cbDiffHighlighting.isSelected());
                            ret.addAll(h);
                        } catch (MissingTemplateException mt) {
                            openMissingTemplatesDialog(mt.getMissingTemplates());

                            //after the user input we try again to print out a translation
                            try {
                                List<Text> h = formattedText(generator.translate_List(finalModels.get(oldidx)), generator.translate_List(finalModels.get(idx)), cbDiffHighlighting.isSelected());
                                ret.addAll(h);
                            } catch (MissingTemplateException mt2) {
                                //if the user did not input the necessary information an error message gets printed out
                                Text t = new Text();
                                t.setText("Can't create translation. \\nMissing templates for: " + mt2.getMissingTemplates());
                                ret.add(t);
                            }
                        }

                        return ret;
                    }
                };

                valueFactory.setValue(1);

                spModels.setValueFactory(valueFactory);

                textModels.setText(models.get(0));
                taModels.getChildren().clear();
                taModels.getChildren().add(textModels);
                this.currentModel = 1;
                try {
                    textCNLOut.setText(generator.translate(models.get(0)));
                    taCNLOut.getChildren().clear();
                    taCNLOut.getChildren().add(textCNLOut);
                } catch (MissingTemplateException mt) {
                    openMissingTemplatesDialog(mt.getMissingTemplates());

                    //after the user input we try again to print out a translation
                    try {
                        textCNLOut.setText(generator.translate(models.get(0)));
                        taCNLOut.getChildren().clear();
                        taCNLOut.getChildren().add(textCNLOut);
                    } catch (MissingTemplateException mt2) {
                        textCNLOut.setText("Can't create translation. \nMissing tempalte for: " + mt2.getMissingTemplates());
                    }
                }
            }
        } catch (DLVException e) {
            textModels.setText(e.getMessage());
        }
    }

    public void cbDiffHighlightingClicked(ActionEvent actionEvent) {
        if (!cbDiffHighlighting.isSelected()) {
            textModels.setText(models.get(currentModel-1));
            taModels.getChildren().clear();
            taModels.getChildren().add(textModels);
            try {
                textCNLOut.setText(generator.translate(models.get(currentModel-1)));
                taCNLOut.getChildren().clear();
                taCNLOut.getChildren().add(textCNLOut);
            } catch (MissingTemplateException mt) {
                openMissingTemplatesDialog(mt.getMissingTemplates());

                //after the user input we try again to print out a translation
                try {
                    textCNLOut.setText(generator.translate(models.get(currentModel-1)));
                    taCNLOut.getChildren().clear();
                    taCNLOut.getChildren().add(textCNLOut);
                } catch (MissingTemplateException mt2) {
                    textCNLOut.setText("Can't create translation. \nMissing tempalte for: " + mt2.getMissingTemplates());
                }
            }
        }
    }

    private static List<Text> formattedAnswerSet(List<String> oldAS, List<String> newAS, boolean diffHighlighting) {
        if (!diffHighlighting) {
            List<Text> list = new LinkedList<>();
            String s = "";

            for (String a : newAS) {
                s += a + "\n";
            }

            list.add(new Text(s));
            return list;
        }
        String ret = "";
        List<Text> formattedTextList = new LinkedList<>();

        List<String> h = new LinkedList<>(oldAS);
        for (String s : h) {
            if (newAS.contains(s)) {
                ret += s + "\n";
                newAS.remove(s);
                oldAS.remove(s);
            }
        }

        Text same = new Text();
        same.setText(ret);

        ret = "";
        for (String s : newAS) {
            ret += s + "\n";
        }
        Text newT = new Text();
        newT.setText(ret);
        newT.setStyle("-fx-fill: RED;");

        formattedTextList.add(newT);
        formattedTextList.add(same);
        return formattedTextList;
    }

    private static List<Text> formattedText(List<String> oldList, List<String> newList, boolean diffHighlighting) {
        if (!diffHighlighting) {
            List<Text> list = new LinkedList<>();
            String s = "";

            for (String a : newList) {
                s += a + "\n";
            }

            list.add(new Text(s));
            return list;
        }
        List<Text> ret_same = new LinkedList<>();
        List<Text> ret_diff = new LinkedList<>();

        List<String> h = new LinkedList<>(oldList);
        for (String s : h) {
            if (newList.contains(s)) {
                Text t = new Text(s + "\n");
                ret_same.add(t);
                newList.remove(s);
                oldList.remove(s);
            }
        }

        Comparator<String> comparator = (s, t1) -> t1.length() - s.length();
        oldList.sort(comparator);
        newList.sort(comparator);

        if (newList.size() < oldList.size()) {
            int j, i;
            for (i = 0; i < newList.size(); i++) {
                int maxEqu = 0;
                int idx = -1;
                String newString = newList.get(i);
                for (j = 0; j < oldList.size(); j++) {
                    int diff = 0;
                    String oldString = oldList.get(j);
                    String diffString = difference(oldString, newString);

                    if (diffString.equals(oldString)) {
                        continue;
                    }

                    diff = oldString.length() - diffString.length();
                    if (diff > maxEqu) {
                        maxEqu = diff;
                        idx = j;
                    }
                }

                if (idx >= 0) {
                    String oldString = oldList.get(idx);
                    List<Text> add = compareStrings(oldString.split(" "), newString.split(" "));
                    if (add != null) {
                        ret_diff.addAll(add);
                    }
                } else {
                    ret_diff.add(new Text(newString));
                }
            }
        } else {
            int j, i;
            for (i = 0; i < oldList.size(); i++) {
                int maxEqu = 0;
                int idx = -1;
                String oldString = oldList.get(i);
                for (j = 0; j < newList.size(); j++) {
                    int diff = 0;
                    String newString = newList.get(j);
                    String diffString = difference(oldString, newString);

                    if (diffString.equals(newString)) {
                        continue;
                    }

                    diff = newString.length() - diffString.length();
                    if (diff > maxEqu) {
                        maxEqu = diff;
                        idx = j;
                    }
                }

                if (idx >= 0) {
                    String newString = newList.get(idx);
                    List<Text> add = compareStrings(oldString.split(" "), newString.split(" "));
                    if (add != null) {
                        ret_diff.addAll(add);
                        newList.remove(idx);
                    }
                }
            }
            for (String s : newList) {
                Text t = new Text(s);
                t.setStyle("-fx-fill: RED;");
                ret_diff.add(t);
            }
        }

        ret_diff.addAll(ret_same);
        return ret_diff;
    }

    private static List<Text> compareStrings(String[] oString, String[] nString) {
        String l = nString[nString.length-1];
        l = (l.endsWith(".") ? l.substring(0, l.length()-1) : l);
        nString[nString.length-1] = l;
        l = oString[oString.length-1];
        l = (l.endsWith(".") ? l.substring(0, l.length()-1) : l);
        oString[oString.length-1] = l;
        int i;
        for (i = 0; i < nString.length && i < oString.length && !nString[i].equals(oString[i]); i++);
        if (i == oString.length || i == nString.length) {
            return null;
        }

        List<Text> ret = new LinkedList<>();
        int oldIdx, newIdx;
        for (oldIdx = 0, newIdx = 0; oldIdx < oString.length && newIdx < nString.length; ) {
            if (oString[oldIdx].equals(nString[newIdx])) {
                String s = nString[newIdx] + " ";
                Text t = new Text(s);
                ret.add(t);
                oldIdx++;
                newIdx++;
            } else {
                //different word
                if (nString.length < oString.length) {
                    //peek the old string array
                    boolean hasMatch = false;
                    for (int j = oldIdx; j < oString.length; j++) {
                        if (oString[j].equals(nString[newIdx])) {
                            hasMatch = true;
                            break;
                        }
                    }
                    if (hasMatch) {
                        for (; oldIdx < oString.length; oldIdx++) {
                            if (oString[oldIdx].equals(nString[newIdx])) {
                                break;
                            }
                        }
                    } else {
                        String s = "";
                        for(; newIdx < nString.length; newIdx++) {
                            s += nString[newIdx] + " ";
                        }
                        Text t = new Text(s);
                        t.setStyle("-fx-fill: RED;");
                        ret.add(t);
                    }
                } else if (nString.length > oString.length){
                    //peek the new string array
                    boolean hasMatch = false;
                    for (int j = newIdx; j < nString.length; j++) {
                        if (oString[oldIdx].equals(nString[j])) {
                            hasMatch = true;
                            break;
                        }
                    }
                    String s = "";
                    if (hasMatch) {
                        for (; newIdx < nString.length; newIdx++) {
                            if (oString[oldIdx].equals(nString[newIdx])) {
                                break;
                            }
                            s += nString[newIdx] + " ";
                        }
                        Text t = new Text(s);
                        t.setStyle("-fx-fill: RED;");
                        ret.add(t);
                    } else {
                        for(; newIdx < nString.length; newIdx++) {
                            s += nString[newIdx] + " ";
                        }
                        Text t = new Text(s);
                        t.setStyle("-fx-fill: RED;");
                        ret.add(t);
                    }
                } else { //new length == old length
                    while(newIdx < nString.length) {
                        String s = "";
                        for(; newIdx < nString.length && !nString[newIdx].equals(oString[newIdx]); newIdx++) {
                            s += nString[newIdx] + " ";
                        }
                        Text t = new Text(s);
                        t.setStyle("-fx-fill: RED;");
                        ret.add(t);
                        s = "";
                        for(; newIdx < nString.length && nString[newIdx].equals(oString[newIdx]); newIdx++) {
                            s += nString[newIdx] + " ";
                        }
                        ret.add(new Text(s));
                    }
                }
            }

        }

        if (newIdx < nString.length) {
            String s = "";
            for (; newIdx < nString.length; newIdx++) {
                s += nString[newIdx] + " ";
            }
            Text t = new Text(s);
            t.setStyle("-fx-fill: RED;");
            ret.add(t);
        }

        String h = ret.get(ret.size()-1).getText();
        while (h.isEmpty()) {
            ret.remove(ret.size()-1);
            h = ret.get(ret.size()-1).getText();
        }
        h = (h.endsWith(" ") ? h.substring(0, h.length()-1) : h);
        ret.get(ret.size()-1).setText(h);
        ret.add(new Text(".\n"));
        return ret;
    }

    public static String difference(String str1, String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str2;
        }
        int at = indexOfDifference(str1, str2);
        if (at == INDEX_NOT_FOUND) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return INDEX_NOT_FOUND;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }
        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); i++) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }
        return INDEX_NOT_FOUND;
    }

    private static String printArray(String[] a) {
        String s = "[";
        for (String sub :
                a) {
            s += sub + ", ";
        }
        return s + "]";
    }

    private void openMissingTemplatesDialog(List<String> atomList) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/missing_templates.fxml"));
            Stage stage = new Stage();

            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(taCNLOut.getScene().getWindow());
            stage.setScene(new Scene(loader.load(), 600, 190));
            stage.setTitle("Missing Template");
            MissingTemplatesDialog missingTemplatesDialog = (MissingTemplatesDialog) loader.getController();
            missingTemplatesDialog.setAtomList(atomList);
            stage.showAndWait();
            this.generator.addDefaultTemplates(missingTemplatesDialog.getMissingTemplates());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCurrentModelStringExport() {
        if (currentModel == -1) {
            return "";
        }
        String h = models.get(currentModel-1);
        h = h.replaceAll("\n", ", ");
        h = h.trim();
        h = h.substring(0, h.length()-1);
        String p = "Model: {" + h + "}\n";

        p += "\n";
        try {
            p += generator.translate(this.models.get(currentModel-1));
        } catch (MissingTemplateException mi) {
            openMissingTemplatesDialog(mi.getMissingTemplates());

            try {
                p += generator.translate(this.models.get(currentModel-1));
            } catch (MissingTemplateException mi2) {
                p += "Can't create translation. \nMissing templates for: " + mi2.getMissingTemplates();
            }
        }
        return p;
    }

    public String getAllModelStringExport() {
        String ret = "";
        if (this.models == null) {
            return "";
        }

        int i = 0, s = this.models.size();
        for (String model : this.models) {
            String h = model.replaceAll("\n", ", ");
            h = h.trim();
            h = h.substring(0, h.length()-1);
            String p = "Model " + (i+1) + "/" + s + ": {" + h + "}\n";
            p += "\n";

            try {
                p += generator.translate(this.models.get(i));
            } catch (MissingTemplateException mt) {
                openMissingTemplatesDialog(mt.getMissingTemplates());

                try {
                    p += generator.translate(this.models.get(i));
                } catch (MissingTemplateException mt2) {
                    p += "Can't create translation. \nMissing templates for: " + mt2.getMissingTemplates();
                }
            }

            ret += p + "\n\n";
            i++;
        }

        return ret;
    }

    public void tfCnlOnKeyReleased(KeyEvent keyEvent) {

        if(!caCNL.getText().equals(initialCNLContent) &&
                (!keyEvent.isShortcutDown() || (keyEvent.getText().equals("v") && (keyEvent.isMetaDown())))){
            highlightTabLabel(true);
        } else {
            highlightTabLabel(false);
        }

        if(mainGuiService.getTranslationType() == TranslationType.AUTOMATIC) {
            if (keyEvent.getText().equals(".") ||
                    (caCNL.getCaretPosition() <= caCNL.getText().lastIndexOf('.') && !keyEvent.getText().equals("")) ||
                    (keyEvent.getText().equals("v") && (keyEvent.isMetaDown() || keyEvent.isControlDown())) ||  // check CMD + V
                    keyEvent.getCode().equals(KeyCode.BACK_SPACE) || keyEvent.getCode().equals(KeyCode.DELETE)) {
                translate();
            }
        }
    }

    Thread thread;

    public void translate() {


        translationTabController.startTranslation();

        TranslatorThread translatorThread;

        translatorThread = new TranslatorThread(this,caCNL.getText());

        if(thread == null)
        {
            thread = new Thread(translatorThread);
            thread.start();
        } else {
            thread.stop();
            thread = new Thread(translatorThread);
            thread.start();
        }
    }

    public void updateCaAspAsync(String s){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                caASP.replaceText(s);
            }
        });
    }

    public void updateTaErrorAsync(String s){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                taError.setText(s);
            }
        });
    }

    public void appendTaErrorAsync(String s){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                taError.appendText(s);
            }
        });
    }

    private void loadSentencePatterns() throws IOException {

        WebEngine webEngine = wvSentencePatterns.getEngine();

        InputStream inputStream = this.getClass().getResourceAsStream("/sentences.htm");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String htmlSource ="";
        String line = "";
        while ((line = bufferedReader.readLine()) != null)
        {
            htmlSource += line;
        }

        webEngine.loadContent(htmlSource);
    }

    public void startTranslation(){
        btnTranslate.setDisable(true);
        btnTranslate.setText("Translating ...");
    }

    public void endTranslationAsync() {
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                btnTranslate.setDisable(false);
                btnTranslate.setText("Translate");
            }
        });
    }

    public void highlightTabLabel(boolean highlight){
        if(!tab.getText().equals(""))
        {
            tabLabel = tab.getText();
            tab.setText("");
        }

        tab.setGraphic(new Label(tabLabel));
        if(highlight == true) {
            tab.getGraphic().setStyle("-fx-text-fill: #0032B2;");
        }else {
            tab.getGraphic().setStyle("-fx-text-fill: black;");
        }
    }

    public boolean hasCnlContentChanged(){
        if(caCNL.getText().equals(initialCNLContent)){
            return false;
        }

        return true;
    }


    public void setMainGuiService(IMainGuiService mainGuiService) {
        this.mainGuiService = mainGuiService;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
        initialCNLContent = caCNL.getText();
    }

    public void setTab(Tab tab) {
        this.tab = tab;
    }

    public Tab getTab() {
        return tab;
    }

    public void setInitialCNLContent(String initialCNLContent) {
        this.initialCNLContent = initialCNLContent;
    }
}
