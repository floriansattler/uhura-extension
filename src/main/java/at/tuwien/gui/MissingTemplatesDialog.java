package at.tuwien.gui;

import at.tuwien.ASPtoNL.templates.DefaultTemplate;
import at.tuwien.ASPtoNL.templates.ISentenceTemplate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MissingTemplatesDialog implements Initializable {

    @FXML
    private Label lbAtom;
    @FXML
    private Button btnNext;
    @FXML
    private TextField tfNormalTemplate, tfNegatedTemplate;

    private List<ISentenceTemplate> missingTemplates;
    private List<String> atomList;

    private int currentAtom = 0;

    private final String PAT = "([A-Z] | [A-Z] | [A-Z]\\.)";
    private Pattern pattern = Pattern.compile(PAT);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void setAtomList(List<String> atomList) {
        this.atomList = atomList;
        lbAtom.setText(generateAtom(atomList.get(currentAtom), this.atomList.get(currentAtom).split(",").length));
        if (currentAtom+1 == this.atomList.size()) {
            btnNext.setText("Finish");
        }
        this.missingTemplates = new ArrayList<>(atomList.size());
    }

    public void btnNextClicked(ActionEvent actionEvent) {
        int varCnt = this.atomList.get(currentAtom).split(",").length;
        if (checkNormalTemplate(varCnt) || checkNegatedTemplate(varCnt)) {
            String e = "The number of variables in the template sentence is not the same as in the atom.";
            ErrorDialog.showErrorDialog(e);
            return;
        }

        this.missingTemplates.add(createTemplate());

        if (currentAtom+1 == atomList.size()) {
            closeStage();
            return;
        }

        varCnt = this.atomList.get(currentAtom+1).split(",").length;
        lbAtom.setText(generateAtom(atomList.get(currentAtom+1), varCnt));
        tfNormalTemplate.setText("");
        tfNegatedTemplate.setText("");

        if (currentAtom+2 == atomList.size()) {
            btnNext.setText("Finish");
        }
        currentAtom++;
    }

    private ISentenceTemplate createTemplate() {
        return new DefaultTemplate(tfNormalTemplate.getText(),
                tfNegatedTemplate.getText(),
                retrievePred(atomList.get(currentAtom)));
    }

    private String retrievePred(String a) {
        if (a.startsWith("-")) {
            return a.substring(1, a.indexOf("("));
        }
        return a.substring(0, a.indexOf("("));
    }

    private boolean checkNormalTemplate(int varCnt) {
        return (varCnt != cntVariables(tfNormalTemplate.getText()));
    }

    private boolean checkNegatedTemplate(int varCnt) {
        return  (varCnt != cntVariables(tfNegatedTemplate.getText()));
    }

    private int cntVariables(String t) {
        Matcher matcher = pattern.matcher(t);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        return i;
    }

    private void closeStage() {
        Stage stage = (Stage) tfNormalTemplate.getScene().getWindow();
        stage.close();
    }

    public void btnCancelClicked(ActionEvent actionEvent) {
        this.missingTemplates.clear();
        closeStage();
    }

    public List<ISentenceTemplate> getMissingTemplates() {
        return missingTemplates;
    }

    private String generateAtom(String atom, int varCnt) {
        String r = (atom.startsWith("-") ? atom.substring(1,atom.indexOf("(")) : atom.substring(0, atom.indexOf("(")));
        int c = 65, offset = 0;

        String ra = "" + (char)(c + (offset++ % 26));

        for (int i = 1; i < varCnt; i++) {
            ra += "," + (char)(c + (offset++ % 26));
        }

        return r + "(" + ra + ")";
    }
}
