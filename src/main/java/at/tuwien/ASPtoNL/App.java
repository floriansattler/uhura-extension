package at.tuwien.ASPtoNL;

import at.tuwien.ASPtoNL.exceptions.MissingTemplateException;
import generatedANTLR4.CNLLexer;
import generatedANTLR4.CNLParser;
import javafx.scene.text.Text;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.*;

public class App {

    public static void main(String[] args) {
        Set<String> as = new HashSet<>(Arrays.asList("node(nodea)", "node(nodeb)", "node(nodec)", "node(noded)", "node(nodee)", "color(red)", "color(green)", "color(blue)", "colored_with(nodea,blue)", "colored_with(nodeb,green)", "colored_with(nodec,red)", "colored_with(noded,green)", "colored_with(nodee,red)", "connected_with(nodea,nodeb)", "connected_with(nodea,nodec)", "connected_with(nodea,noded)", "connected_with(nodea,nodee)", "connected_with(nodeb,nodea)", "connected_with(nodeb,nodec)", "connected_with(nodeb,nodee)", "connected_with(nodec,nodea)", "connected_with(nodec,nodeb)", "connected_with(nodec,noded)", "connected_with(noded,nodea)", "connected_with(noded,nodec)", "connected_with(noded,nodee)", "connected_with(nodee,nodea)", "connected_with(nodee,nodeb)", "connected_with(nodee,noded)"));
        Set<String> cnl = new HashSet<>(Arrays.asList("NodeA is a node.",
                "NodeB is a node.",
                "NodeC is a node.",
                "NodeD is a node.",
                "NodeE is a node.",
                "NodeA is connected with NodeB.",
                "NodeB is connected with NodeC.",
                "NodeC is connected with NodeD.",
                "NodeD is connected with NodeE.",
                "NodeE is connected with NodeA.",
                "NodeA is connected with NodeD.",
                "NodeA is connected with NodeC.",
                "NodeE is connected with NodeB.",
                "Red is a color.",
                "Green is a color.",
                "Blue is a color.",
                "If X is a node then X is colored with Green or X is colored with Red or X is colored with Blue.",
                "Exclude that node X is colored with Z and that node Y is colored with Z and that Z is a color and that X is connected with Y.",
                "If X is connected with Y then Y is connected with X."));
        /*cnl = getCNL4();
        as = getAS4();*/

        /*cnl = new HashSet<>(Arrays.asList("Russel Wilson is a football player.", "If X is a football player then X is smart or X is strong.", "Tim is a chess player."));
        as = new HashSet<>(Arrays.asList("footballplayer(russelwilson)", "strong(russelwilson)", "chessplayer(tim)"));*/

        NLGenerator generator = new NLGenerator(as, cnl);
        System.out.println("PREDICATES: " + generator.getPREDS());
        System.out.println("CNLPREDICATES: " + generator.getCNLPREDS());
        System.out.println("CONSTANTS: " + generator.getCONSTANTS());
        System.out.println("CNLCONSTANTS: " + generator.getCNLCONSTANTS());

        System.out.println("-----------------------------START-----------------------------");

        String text = "";
        for (String s :
                cnl) {
            text += s + "\n";
        }

        System.out.println();

        System.out.println("Preprocessed text: ");
        System.out.println(generator.preprocessCNLText(text));

        CNLLexer lexer = new CNLLexer(CharStreams.fromString(generator.preprocessCNLText(text)), generator.getCNLCONSTANTS(), generator.getCNLPREDS());
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CNLParser parser = new CNLParser(tokens);
        ParseTree tree = parser.text();
        ParseTreeWalker walker = new ParseTreeWalker();
        CNLWalker listener = new CNLWalker(generator.getPREDS());
        walker.walk(listener, tree);
        System.out.println("Count: " + listener.getTemplates().size());
        System.out.println(listener.getTemplates());

        int c = 0;
        try {
            for (String s : generator.getSentences(listener.getTemplates(), as)) {
                System.out.println(s);
                c++;
            }

            System.out.println();

            System.out.println("Count of answer set: " + as.size());
            System.out.println("Count of sentences: " + c);

            System.out.println();
            System.out.println("----------TEST----------");
            CNLPostProcessor postProcessor = new CNLPostProcessor(generator.getSentences(listener.getTemplates(), as));
            //List<String> test = postProcessor.getPostProcessedSentences();
            //Collections.sort(test);
            for (String s : postProcessor.getPostProcessedSentences()) {
                System.out.println(s);
            }

            System.out.println();
            System.out.println("Count of compressed sentences: " + postProcessor.getPostProcessedSentences().size());
            System.out.println("Rate: " + (float) postProcessor.getPostProcessedSentences().size() / (float) c);
        } catch (MissingTemplateException mt) {

        }


        System.out.println("-----TEST-----");
        /*List<String> oldSents = new LinkedList<>(Arrays.asList("Nurse is a job."));
        List<String> newSents = new LinkedList<>(Arrays.asList("Clerk is a job."));*/
        List<String> oldSents = new LinkedList<>(Arrays.asList("Person Robin does not work for person Tim and works for person Robin.",
        "Person Tim works for person Robin and Tim.",
        "Tim and Robin are people."));
        List<String> newSents = new LinkedList<>(Arrays.asList("Person Robin does not work for person Tim and works for person Robin.",
                "Person Tim does not work for person Tim and works for person Robin.",
                "Tim and Robin are people."));

        System.out.println("Old: " + oldSents);
        System.out.println("New: " + newSents);

        System.out.println("----");
        System.out.println(formattedText_newnew(oldSents, newSents));
        System.out.println("----");

        System.out.println("> " + compareStrings("Nurse is a job.".split(" "),
                "Clerk is a job.".split(" ")));

        System.out.println(formattedText(oldSents, newSents));

        for (Text t :
                formattedText_FX(oldSents, newSents)) {
            if (t.getStyle().equals("-fx-fill: RED;")) {
                System.out.print("<changed>");
            } else if (t.getStyle().equals("-fx-fill: GREY;")) {
                System.out.print("<removed>");
            } else if (t.getStyle().equals("-fx-fill: GREEN;")) {
                System.out.print("<new>");
            }
            System.out.print(t.getText());
        }

        /*
         * So for example: <new> is green, <removed> is grey, <changed> is red
         */
    }

    private static List<String> formattedText_newnew(List<String> oldList, List<String> newList) {
        List<String> ret = new LinkedList<>();

        List<String> h = new LinkedList<>(oldList);
        for (String s : h) {
            if (newList.contains(s)) {
                ret.add(s + "\n");
                newList.remove(s);
                oldList.remove(s);
            }
        }

        Comparator<String> comparator = (s, t1) -> t1.length() - s.length();
        oldList.sort(comparator);
        newList.sort(comparator);


        if (newList.size() < oldList.size()) {
            int j, i;
            for(i = 0; i < newList.size(); i++) {
                int maxEqu = 0;
                int idx = -1;
                String newString = newList.get(i);
                for (j = 0; j < oldList.size(); j++) {
                    int diff = 0;
                    String oldString = oldList.get(j);
                    String diffString = difference(oldString, newString);

                    if (diffString.equals(oldString)) {
                        continue;
                    }

                    diff = oldString.length() - diffString.length();
                    if (diff > maxEqu) {
                        maxEqu = diff;
                        idx = j;
                    }
                }

                if (idx >= 0) {
                    String oldString = oldList.get(idx);
                    String r = compareStrings(oldString.split(" "), newString.split(" "));
                    if (!r.isEmpty()) {
                        ret.add(r);
                    }
                } else {
                    ret.add(newString);
                }
            }
        } else {
            int j, i;
            for (i = 0; i < oldList.size(); i++) {
                int maxEqu = 0;
                int idx = -1;
                String oldString = oldList.get(i);
                for (j = 0; j < newList.size(); j++) {
                    int diff = 0;
                    String newString = newList.get(j);
                    String diffString = difference(oldString, newString);

                    if (diffString.equals(newString)) {
                        continue;
                    }

                    diff = newString.length() - diffString.length();
                    if (diff > maxEqu) {
                        maxEqu = diff;
                        idx = j;
                    }
                }

                if (idx >= 0) {
                    String newString = newList.get(idx);
                    String r = compareStrings(oldString.split(" "), newString.split(" "));
                    if (!r.isEmpty()) {
                        ret.add(r);
                        newList.remove(idx);
                    }
                }
            }
            for (String s : newList) {
                ret.add("<new>" + s + "</new>");
            }
        }
        return ret;
    }

    private static String compareStrings(String[] oString, String[] nString) {
        String l = nString[nString.length-1];
        l = (l.endsWith(".") ? l.substring(0, l.length()-1) : l);
        nString[nString.length-1] = l;
        l = oString[oString.length-1];
        l = (l.endsWith(".") ? l.substring(0, l.length()-1) : l);
        oString[oString.length-1] = l;
        int i;
        for (i = 0; i < nString.length && i < oString.length && !nString[i].equals(oString[i]); i++);
        if (i == oString.length || i == nString.length) {
            return "";
        }

        String ret = "";
        i = 0;
        int oldIdx, newIdx;
        for (oldIdx = 0, newIdx = 0; oldIdx < oString.length && newIdx < nString.length; ) {
            if (oString[oldIdx].equals(nString[newIdx])) {
                ret += nString[newIdx] + " ";
                oldIdx++;
                newIdx++;
            } else {
                //different word
                if (nString.length < oString.length) {
                    //peek the old string array
                    boolean hasMatch = false;
                    for (int j = oldIdx; j < oString.length; j++) {
                        if (oString[j].equals(nString[newIdx])) {
                            hasMatch = true;
                            break;
                        }
                    }
                    //if hasMatch is true, we must do a removed tag
                    if (hasMatch) {
                        ret += "<removed>";
                        for (; oldIdx < oString.length; oldIdx++) {
                            if (oString[oldIdx].equals(nString[newIdx])) {
                                break;
                            }
                            ret += oString[oldIdx] + " ";
                        }
                        ret += "</removed>";
                    } else {
                        ret += "<changed>";
                        for(; newIdx < nString.length; newIdx++) {
                            ret += nString[newIdx] + " ";
                        }
                        ret += "</changed>";
                    }
                } else if (nString.length > oString.length){
                    //peek the new string array
                    boolean hasMatch = false;
                    for (int j = newIdx; j < nString.length; j++) {
                        if (oString[oldIdx].equals(nString[j])) {
                            hasMatch = true;
                            break;
                        }
                    }
                    //if hasMatch is true, we must do a new tag
                    if (hasMatch) {
                        ret += "<new>";
                        for (; newIdx < nString.length; newIdx++) {
                            if (oString[oldIdx].equals(nString[newIdx])) {
                                break;
                            }
                            ret += nString[newIdx] + " ";
                        }
                        ret += "</new>";
                    } else {
                        ret += "<changed>";
                        for(; newIdx < nString.length; newIdx++) {
                            ret += nString[newIdx] + " ";
                        }
                        ret += "</changed>";
                    }
                } else { //new length = old length
                    //peek both arrays if they have a match later
                    while (newIdx < nString.length) {
                        ret += "<changed>";
                        for (; newIdx < nString.length && !nString[newIdx].equals(oString[newIdx]); newIdx++) {
                            ret += nString[newIdx] + " ";
                        }
                        ret += "</changed>";
                        for (; newIdx < nString.length && nString[newIdx].equals(oString[newIdx]); newIdx++) {
                            ret += nString[newIdx] + " ";
                        }
                    }
                }
            }

        }

        if (newIdx < nString.length) {
            ret += "<new>";
            for (; newIdx < nString.length; newIdx++) {
                ret += nString[newIdx] + " ";
            }
            ret += "</new>";
        }

        return ret + ".";
    }

    private static List<Text> formattedText_FX(List<String> oldList, List<String> newList) {
        String ret = "";
        List<Text> formattedTextList = new LinkedList<>();

        List<String> h = new LinkedList<>(oldList);
        for (String s : h) {
            if (newList.contains(s)) {
                ret += s + "\n";
                newList.remove(s);
                oldList.remove(s);
            }
        }

        Text t;
        Text same = new Text();
        same.setText(ret);

        ret = "";

        int j, i;
        for (i = 0; i < oldList.size(); i++) {
            int maxEqu = 0;
            int idx = -1;
            String oldString = oldList.get(i);
            for (j = 0; j < newList.size(); j++) {
                int diff = 0;
                String newString = newList.get(j);
                String diffString = difference(oldString, newString);

                if (diffString.equals(newString)) {
                    continue;
                }

                diff = newString.length() - diffString.length();
                if (diff > maxEqu) {
                    maxEqu = diff;
                    idx = j;
                }
            }

            if (idx >= 0) {
                String hh = newList.get(idx).substring(0, indexOfDifference(oldList.get(i), newList.get(idx)));
                String rem = "";
                int ki;
                for (ki = hh.length()-1; ki > 0 && hh.charAt(ki) != ' '; ki--);

                rem = hh.substring(ki);

                hh = hh.substring(0, ki);

                t = new Text();
                t.setText(hh);
                formattedTextList.add(t);

                hh += "<changed>" + difference(oldList.get(i), newList.get(idx)) + "</changed>\n";
                //ret += hh;

                int diffIdx = indexOfDifference(oldList.get(i), newList.get(idx));
                String[] oldRest = oldString.substring(diffIdx).split(" ");
                String[] newRest = newList.get(idx).substring(diffIdx).split(" ");

                boolean open = true;
                int k;
                String s = rem;
                for (k = 0; k < newRest.length && k < oldRest.length; k++) {
                    if (oldRest[k].equals(newRest[k])) {
                        if (open) {
                            t = new Text();
                            t.setText(s);
                            t.setStyle("-fx-fill: RED;");
                            formattedTextList.add(t);
                            s = newRest[k] + " ";
                            open = false;
                        } else {
                            s += newRest[k] + " ";
                        }
                    } else {
                        if (open) {
                            s += newRest[k] + " ";
                        } else {
                            t = new Text();
                            t.setText(s);
                            formattedTextList.add(t);
                            s = newRest[k] + " ";
                            open = true;
                        }
                    }
                }

                if (open) {
                    t = new Text();
                    t.setText(s);
                    t.setStyle("-fx-fill: RED;");
                    formattedTextList.add(t);
                }

                if (k < newRest.length) {
                    t = new Text();
                    String sub = "";
                    for (; k < newRest.length; k++) {
                        sub += newRest[k] + " ";
                    }
                    t.setText(sub);
                    t.setStyle("-fx-fill: RED;");
                    formattedTextList.add(t);
                }

                t = new Text();
                t.setText("\n");
                formattedTextList.add(t);

                newList.remove(idx);
                oldList.remove(i);
                i = -1;
            }
        }

        for (String s : oldList) {
            ret += "<removed>" + s + "</removed>\n";
            t = new Text();
            t.setText(s + "\n");
            t.setStyle("-fx-fill: GREY;");
            formattedTextList.add(t);
        }

        for (String s : newList) {
            t = new Text();
            t.setText(s + "\n");
            t.setStyle("-fx-fill: GREEN;");
            formattedTextList.add(t);
            ret += "<new>" + s + "</new>\n";
        }

        formattedTextList.add(same);

        return formattedTextList;
    }

    private static String formattedText(List<String> oldList, List<String> newList) {
        String ret = "";

        List<String> h = new LinkedList<>(oldList);
        for (String s : h) {
            if (newList.contains(s)) {
                ret += s + "\n";
                newList.remove(s);
                oldList.remove(s);
            }
        }

        int j, i;
        for (i = 0; i < oldList.size(); i++) {
            int maxEqu = 0;
            int idx = -1;
            String oldString = oldList.get(i);
            boolean removed = false;
            for (j = 0; j < newList.size(); j++) {
                int diff = 0;
                String newString = newList.get(j);
                String diffString = difference(oldString, newString);

                if (diffString.equals(newString)) {
                    //removed = (j == newList.size()-1);
                    continue;
                }

                diff = newString.length() - diffString.length();
                if (diff > maxEqu) {
                    maxEqu = diff;
                    idx = j;
                }
            }

            if (idx >= 0) {
                int diffIdx = indexOfDifference(oldList.get(i), newList.get(idx));
                String hh = newList.get(idx).substring(0, diffIdx);

                String[] oldRest = oldString.substring(diffIdx).split(" ");
                String[] newRest = newList.get(idx).substring(diffIdx).split(" ");

                hh += "<changed>";

                boolean open = true;
                int k;
                for (k = 0; k < newRest.length && k < oldRest.length; k++) {
                    if (oldRest[k].equals(newRest[k])) {
                        if (open) {
                            hh += "</changed> " + newRest[k] + " ";
                            open = false;
                        } else {
                            hh += newRest[k] + " ";
                        }
                    } else {
                        if (open) {
                            hh += newRest[k] + " ";
                        } else {
                            hh += "<changed>" + newRest[k] + " ";
                            open = true;
                        }
                    }
                }

                if (open) {
                    hh += "</changed>";
                }

                if (k < newRest.length) {
                    hh += "<new>";
                    for (; k < newRest.length; k++) {
                        hh += newRest[k] + " ";
                    }
                    hh += "</new>";
                }

                ret += hh + "\n";
                newList.remove(idx);
                oldList.remove(i);
                i = -1;
            }
        }

        for (String s :
                oldList) {
            ret += "<removed>" + s + "</removed>\n";
        }

        for (String s :
                newList) {
            ret += "<new>" + s + "</new>\n";
        }

        return ret;
    }

    private static final int INDEX_NOT_FOUND = -1;

    public static String difference(String str1, String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str2;
        }
        int at = indexOfDifference(str1, str2);
        if (at == INDEX_NOT_FOUND) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return INDEX_NOT_FOUND;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }
        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); i++) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }
        return INDEX_NOT_FOUND;
    }

    private static String printArray(String[] a) {
        String s = "[";
        for (String sub :
                a) {
            s += sub + ", ";
        }
        return s + "]";
    }

    private static Set<String> getAS() {
        return new HashSet<>(
                Arrays.asList("educated(roberta)",
                        "educated(steve)",
                        "person(roberta)",
                        "person(thelma)",
                        "person(steve)",
                        "person(pete)",
                        "husband(pete,thelma)",
                        "job(chef)",
                        "job(guard)",
                        "job(nurse)",
                        "job(clerk)",
                        "job(policeofficer)",
                        "job(teacher)",
                        "job(actor)",
                        "job(boxer)",
                        "female(roberta)",
                        "female(thelma)",
                        "male(steve)",
                        "male(pete)",
                        "-hold(roberta,chef)",
                        "hold(thelma,chef)",
                        "-hold(steve,chef)",
                        "-hold(pete,chef)",
                        "hold(roberta,guard)",
                        "-hold(thelma,guard)",
                        "-hold(steve,guard)",
                        "-hold(pete,guard)",
                        "-hold(roberta,nurse)",
                        "-hold(thelma,nurse)",
                        "hold(steve,nurse)",
                        "-hold(pete,nurse)",
                        "-hold(roberta,clerk)",
                        "-hold(thelma,clerk)",
                        "-hold(steve,clerk)",
                        "hold(pete,clerk)",
                        "-hold(roberta,policeofficer)",
                        "-hold(thelma,policeofficer)",
                        "hold(steve,policeofficer)",
                        "-hold(pete,policeofficer)",
                        "hold(roberta,teacher)",
                        "-hold(thelma,teacher)",
                        "-hold(steve,teacher)",
                        "-hold(pete,teacher)",
                        "-hold(roberta,actor)",
                        "-hold(thelma,actor)",
                        "-hold(steve,actor)",
                        "hold(pete,actor)",
                        "-hold(roberta,boxer)",
                        "hold(thelma,boxer)",
                        "-hold(steve,boxer)",
                        "-hold(pete,boxer)")
        );
    }

    private static Set<String> getCNL() {
        return new HashSet<>(
                Arrays.asList("Roberta is a person.",
                        "Thelma is a person.",
                        "Steve is a person.",
                        "Pete is a person.",
                        "Roberta is female.",
                        "Thelma is female.",
                        "Steve is male.",
                        "Pete is male.",
                        "Exclude that person X is male and that person X is female.",
                        "If there is a person X and there is a job Y then person X holds job Y or person X does not hold job Y.",
                        "Exclude that there is a job Y and that person X holds more than one job Y.",
                        "Exclude that there is a job Y and that person X holds less than one job Y.",
                        "Exclude that there is a person X and that person X holds more than two jobs Y.",
                        "Exclude that there is a person X and that person X holds less than two jobs Y.",
                        "Chef is a job.",
                        "Guard is a job.",
                        "Nurse is a job.",
                        "Clerk is a job.",
                        "Police officer is a job.",
                        "Teacher is a job.",
                        "Actor is a job.",
                        "Boxer is a job.",
                        "If a person X holds a job as actor then person X is male.",
                        "If a person X holds a job as nurse then person X is male.",
                        "If a person X holds a job as chef and a person Y holds a job as clerk then a person Y is a husband of a  person X.",
                        "If a person X is a husband of a person Y then person X is male.",
                        "If a person X is a husband of a person Y then person Y is female.",
                        "Exclude that Roberta holds a job as boxer.",
                        "Exclude that Pete is educated.",
                        "If a person X holds a job as nurse then person X is educated.",
                        "If a person X holds a job as police officer then person X is educated.",
                        "If a person X holds a job as teacher then person X is educated.",
                        "Exclude that Roberta holds a job as chef.",
                        "Exclude that Roberta holds a job as police officer.",
                        "Exclude that a person X holds a job as chef and that person X holds a job as police officer.")
        );
    }

    private static Set<String> getAS2() {
        return new HashSet<>(Arrays.asList("venusian(ork)",
                "person(ork)",
                "person(bog)",
                "martian(bog)",
                "from(bog,mars)",
                "from(ork,venus)",
                "female(bog)",
                "liar(ork)",
                "liar(bog)",
                "male(ork)"));
    }

    private static Set<String> getCNL2() {
        return new HashSet<>(Arrays.asList("If X is Martian then X is from Mars.",
                "Exclude that X is a Venusian and that X is from Mars.",
                "If X is a Venusian then X is from Venus.",
                "Exclude that X is a Martian and that X is from Venus.",
                "If X is a person then person X is a Martian or person X is a Venusian. ",
                "If X is a person then person X is female or Person X is male.",
                "If person X is female and person X is a Martian then person X is a liar.",
                "If person X is male and person X is a Venusian then person X is a liar.",
                "If person X is male and person X is a Martian then person X is truthful.",
                "If person X is female and person X is a Venusian then person X is truthful.",
                "Exclude that X is a person and that X is a liar and that X is trusthful.  ",
                "Ork is a person.",
                "Bog is a person. ",
                "If Ork is truthful then Bog is from Venus.",
                "Exclude that Bog is from Venus and that Ork is a liar.",
                "If Bog is truthful then Ork is from Mars.  ",
                "Exclude that Bog is a liar and that Ork is from Mars.  ",
                "If Ork is truthful then Bog is male.",
                "Exclude that Ork is a liar and that Bog is male.  ",
                "If Bog is truthful then Ork is female.",
                "Exclude that Bog is a liar and that Ork is female."));
    }

    private static Set<String> getCNL3() {
        return new HashSet<>(Arrays.asList("Sam is a child.",
                "If X is a father then X is a parent.",
                "If X is mother of child Y then X is a parent of Y.",
                "John does not care about Sam.",
                "Alice is absent."));
    }

    private static Set<String> getAS3() {
        return new HashSet<>(Arrays.asList("absent(alice)", "-care_about(john,sam)", "child(sam)"));
    }

    private static Set<String> getCNL4() {
        return new HashSet<>(Arrays.asList("If X is a student and X works then X is successful.",
                "If student X studies at Macquarie University then student X works or student X celebrates.",
                "Exclude that student X is enrolled in Information Technology and that student X celebrates.",
                "Tom is a student.",
                "Tom studies at Macquarie University.",
                "Tom is enrolled in Information Technology.",
                "Bob is a student.",
                "Bob studies at Macquarie.",
                "Bob does not work."));
    }

    private static Set<String> getAS4() {
        return new HashSet<>(Arrays.asList("enrolled_in(tom,informationtechnology)",
                "study_at(tom,macquarieuniversity)",
                "study_at(bob,macquarie)",
                "student(tom)",
                "student(bob)",
                "-work(bob)",
                "work(tom)",
                "successful(tom)"));
    }
}
