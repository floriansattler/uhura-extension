package at.tuwien.ASPtoNL;

public final class Pluralizer {

    /**
     * Code adapted from https://codegolf.stackexchange.com/questions/112077/convert-singular-to-plural
     * @param s word to be pluralized
     * @return the pluralized version of word s
     */
    public static String pluralize(String s){
        boolean firstLetterUp = (Character.isUpperCase(s.charAt(0)));
        String h = s.toLowerCase();

        //additional: handle irregular nouns
        switch (h) {
            case "woman":
            case "aircraft":
            case "bison":
                return convertReturning(h, firstLetterUp);
            case "man":
                return convertReturning("men", firstLetterUp);
            case "person":
                return convertReturning("people", firstLetterUp);
            case "child":
                return convertReturning("children", firstLetterUp);
            case "mouse":
                return convertReturning("mice", firstLetterUp);
            case "foot":
                return convertReturning("feet", firstLetterUp);
            case "goose":
                return convertReturning("geese", firstLetterUp);
            case "tooth":
                return convertReturning("teeth", firstLetterUp);
            case "louse":
                return convertReturning("lice", firstLetterUp);
            case "cactus":
                return convertReturning("cacti", firstLetterUp);
            case "appendix":
                return convertReturning("appendices", firstLetterUp);
            case "ox":
                return convertReturning("oxen", firstLetterUp);
            case "alumna":
                return convertReturning("alumnae", firstLetterUp);
            case "alumnus":
                return convertReturning("alumni", firstLetterUp);
            case "analysis":
                return convertReturning("analyses", firstLetterUp);
            case "antithesis":
                return convertReturning("antitheses", firstLetterUp);
            case "axis":
                return convertReturning("axes", firstLetterUp);
            case "bacterium":
                return convertReturning("bacteria", firstLetterUp);
            case "basis":
                return convertReturning("bases", firstLetterUp);
            case "crisis":
                return convertReturning("crises", firstLetterUp);
            case "criterion":
                return convertReturning("criteria", firstLetterUp);
            case "datum":
                return convertReturning("data", firstLetterUp);
            case "diagnosis":
                return convertReturning("diagnoses", firstLetterUp);
        }

        // Consonant at the 2nd last position?
        boolean b = "bcdfghjklmnpqrstvwxyzs".contains(String.valueOf(s.charAt(s.length()-2)));

        // Substring for cases where last letter needs to be replaced
        String x = s.substring(0,s.length()-1);

        if(s.endsWith("s") || s.endsWith("x") || s.endsWith("z") || s.endsWith("ch") || s.endsWith("sh"))
            return s + "es";
        if(s.endsWith("y") && b)
            return x + "ies";
        if(s.endsWith("f"))
            return x + "ves";
        if(s.endsWith("fe"))
            return s.substring(0,s.length()-2) + "ves";
        if(s.endsWith("o") && b)
            return s + "es";

        return s += "s";
    }

    private static String convertReturning(String w, boolean firstLetterUp) {
        if (firstLetterUp) {
            return Character.toUpperCase(w.charAt(0)) + w.substring(1);
        } else {
            return w;
        }
    }

}
