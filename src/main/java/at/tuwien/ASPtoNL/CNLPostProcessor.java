package at.tuwien.ASPtoNL;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class CNLPostProcessor {

    private List<String> sentences;

    private List<String> postSentences;

    public CNLPostProcessor(List<String> sentences) {
        this.sentences = sentences;
        postSentences = null;
    }

    public List<String> getPostProcessedSentences() {
        if (postSentences != null) {
            return postSentences;
        }

        List<String> postSent = new LinkedList<>();

        int sentIdx = 0;
        while (sentIdx < sentences.size()) {
            //CASE: last sentence
            if (sentIdx == sentences.size()-1) {
                postSent.add(sentences.get(sentIdx));
                break;
            }

            String curr = sentences.get(sentIdx);
            String next = sentences.get(sentIdx+1);

            int idx = getMatcherIdx(curr, next);
            if (idx == 0) {
                postSent.add(sentences.get(sentIdx));
                sentIdx++;
            } else {
                String start = getSentStart(curr, idx);
                int unionCnt = 0, i;
                for (i = sentIdx; i+1 < sentences.size() && sentences.get(i+1).startsWith(start); i++);
                unionCnt = i - sentIdx;

                String union = start + getBackPart(curr, start);
                for (int j = 1; j < unionCnt; j++) {
                    union += ", " + getBackPart(sentences.get(sentIdx + j), start);
                }
                union += (start.contains(" not ") ? (unionCnt > 1 ? ", or " : " or ") : (unionCnt > 1 ? ", and " : " and ")) + getBackPart(sentences.get(sentIdx + unionCnt), start) + ".";
                postSent.add(union);
                sentIdx += unionCnt + 1;
            }
        }

        this.postSentences = postSent;

        transformIsASentences();

        return this.postSentences;
    }

    private void transformIsASentences() {
        List<String> isasents = new LinkedList<>();
        List<String> sumWords = new LinkedList<>();
        for (String s : new LinkedList<>(this.postSentences)) {
            if (s.matches("[A-Za-z0-9]+( [A-Za-z0-9]+)? is( a)? [A-Za-z0-9]+\\.")) {
                isasents.add(s);
                this.postSentences.remove(s);
            }
        }

        Collections.sort(isasents, Comparator.comparing(s -> new StringBuilder(s).reverse().toString()));

        for (int i = 0; i < isasents.size()-1; i++) {
            String currS = isasents.get(i);
            String curr = currS.substring(currS.lastIndexOf(" ")+1);
            sumWords.clear();

            for (String s : new LinkedList<String>(isasents)) {
                if (s.endsWith(curr)) {
                    isasents.remove(s);
                    sumWords.add(s.substring(0,s.indexOf(" is")));
                }
            }

            if (sumWords.size() <= 1) {
                this.postSentences.add(currS);
                continue;
            }

            String addS = "";
            for (int j = 0; j < sumWords.size()-1; j++) {
                addS += sumWords.get(j) + (j == sumWords.size()-2 ? (sumWords.size() < 3 ? " " : ", ") : ", ");
            }

            addS += "and " + sumWords.get(sumWords.size()-1);

            currS = currS.substring(currS.indexOf(" ") + 1);
            String append = "are ";

            if (currS.contains(" a ") || currS.contains(" an ")) {
                append += Pluralizer.pluralize(currS.substring(currS.lastIndexOf(" ")+1, currS.length()-1)) + ".";
            } else {
                append += currS.substring(currS.lastIndexOf(" ") + 1);
            }

            addS += " " + append;
            this.postSentences.add(addS);
        }

        this.postSentences.addAll(isasents);
    }

    private String getBackPart(String s, String start) {
        String r = s.replaceAll(start, "");
        return (r.endsWith(".") ? r.substring(0,r.length()-1) : r);
    }

    private String getSentStart(String s, int idx) {
        String r = "";
        String[] w = s.split(" ");
        for (int i = 0; i < idx; i++) {
            r += w[i] + " ";
        }
        return r;
    }

    private int getMatcherIdx(String a, String b) {
        String[] aWords = a.split(" ");
        String[] bWords = b.split(" ");

        int i = 0;
        for (; i < aWords.length && aWords[i].equals(bWords[i]); i++);

        int sidx = sentences.indexOf(b);
        if (sidx < sentences.size()-1) {
            //peek next sentence
            String[] cWords = sentences.get(sidx+1).split(" ");
            int j = 0;
            for (; j < bWords.length && bWords[j].equals(cWords[j]); j++);

            if (j > i) {
                return 0;
            }
        }

        return i;
    }
}
