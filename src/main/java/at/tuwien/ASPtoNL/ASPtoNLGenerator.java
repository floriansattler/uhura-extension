package at.tuwien.ASPtoNL;

import at.tuwien.ASPtoNL.exceptions.MissingTemplateException;
import at.tuwien.ASPtoNL.templates.ISentenceTemplate;
import generatedANTLR4.CNLLexer;
import generatedANTLR4.CNLParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ASPtoNLGenerator {

    private String text = "";
    private Set<String> as;
    private Set<String> cnl;

    private List<ISentenceTemplate> defaultTemplates;
    private Set<ISentenceTemplate> templates;

    private NLGenerator generator;

    public ASPtoNLGenerator(String cnltext, String cnlprogram, List<ISentenceTemplate> defaultTemplates) {
        this.as = getAtoms(cnlprogram);
        cnltext = cnltext.replaceAll("\n", "");
        String[] sents = cnltext.split("\\.");
        this.cnl = new HashSet<>();
        for (String s : sents) {
            if (!s.trim().startsWith("%")) {
                this.cnl.add(s + ".");
                this.text += s + ".\n";
            }
        }
        this.templates = new HashSet<>();
        this.generator = new NLGenerator(this.as, this.cnl);

        this.defaultTemplates = (defaultTemplates == null ? new LinkedList<>() : defaultTemplates);

        this.parseText();
    }

    private Set<String> getAtoms(String prog) {
        final String PAT = "([a-z]+_)?[a-z]+\\(([A-Z]|[a-z-_0-9]+)(,([A-Z]|[a-z-_0-9]+))*\\)";
        Pattern pattern = Pattern.compile(PAT);
        Matcher matcher = pattern.matcher(prog);

        Set<String> ret = new HashSet<>();

        while (matcher.find()) {
            ret.add(matcher.group());
        }

        return ret;
    }

    public String translate(String answerset) throws MissingTemplateException{
        if (answerset.isEmpty()) {
            return "";
        }
        String[] as = answerset.split("\n");
        for (int i = 0; i < as.length; i++) {
            as[i] = (as[i].endsWith(".") ? as[i].substring(0,as[i].length()-1) : as[i]);
        }
        this.templates.addAll(this.defaultTemplates);
        CNLPostProcessor postProcessor = new CNLPostProcessor(this.generator.getSentences(this.templates, new HashSet<>(Arrays.asList(as))));

        String ret = "";
        for (String s : postProcessor.getPostProcessedSentences()) {
            ret += s + "\n";
        }
        return ret;
    }

    public List<String> translate_List(String answerset) throws MissingTemplateException {
        String[] as = answerset.split("\n");
        for (int i = 0; i < as.length; i++) {
            as[i] = (as[i].endsWith(".") ? as[i].substring(0,as[i].length()-1) : as[i]);
        }
        this.templates.addAll(this.defaultTemplates);
        CNLPostProcessor postProcessor = new CNLPostProcessor(this.generator.getSentences(this.templates, new HashSet<>(Arrays.asList(as))));
        return postProcessor.getPostProcessedSentences();
    }

    public void addDefaultTemplates(Collection<ISentenceTemplate> defaults) {
        this.defaultTemplates.addAll(defaults);
    }

    private void parseText() {
        Set<ISentenceTemplate> templates = new HashSet<>();

        for (String s : this.cnl) {
            CNLLexer lexer = new CNLLexer(CharStreams.fromString(generator.preprocessCNLText(s)), generator.getCNLCONSTANTS(), generator.getCNLPREDS());
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            CNLParser parser = new CNLParser(tokens);
            ParseTree tree = parser.sentence();
            ParseTreeWalker walker = new ParseTreeWalker();
            CNLWalker listener = new CNLWalker(generator.getPREDS());
            walker.walk(listener, tree);

            templates.addAll(listener.getTemplates());
        }

        this.templates.addAll(templates);
    }
}
