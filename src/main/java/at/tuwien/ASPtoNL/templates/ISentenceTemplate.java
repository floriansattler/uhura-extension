package at.tuwien.ASPtoNL.templates;

public interface ISentenceTemplate {

    String getSentence(String... p);

    String getSentence();

    String getNegatedSentence(String... p);

    String getNegatedSentence();

    String getMainIdentifier();

    String getIdentifier();

    Integer getScore();
}
