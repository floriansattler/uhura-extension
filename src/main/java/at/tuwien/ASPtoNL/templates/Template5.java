package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.getSentStart;

/**
 * This class stores the sentence with the following pattern:
 * [C | Pred VAR] Pred MOLE NUMB Pred VAR
 */
public class Template5 extends SentenceTemplate {

    private final Placeholder ph_front;
    private final String PRED;
    private final String IDENTIFIER_PRED;
    private final Placeholder ph_back;

    private final int SCORE;

    public Template5(Placeholder ph_front, String PRED, String IDENTIFIER_PRED, Placeholder ph_back) {
        this.ph_front = ph_front;
        this.PRED = PRED;
        this.ph_back = ph_back;
        this.IDENTIFIER_PRED = IDENTIFIER_PRED;
        this.SCORE = generateScore();
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    private int generateScore() {
        int s = 1;
        switch (ph_front.getType()) {
            case CONSTANT:
                s += 2;
                break;
            case PRED_VAR:
                s += 1;
                break;
        }
        return s;
    }

    @Override
    public String getSentence(String... p) {
        if (p.length < 2) {
            return getSentStart(ph_front.getString(p[0])) + " " + PRED.replaceAll("_", " ") + " " + getPhBack(p) + ".";
        }
        return getSentStart(ph_front.getString(p[0])) + " " + PRED.replaceAll("_", " ") + " " + ph_back.getString(p[1]).toLowerCase() + ".";
    }

    @Override
    public String getSentence() {
        return getSentStart(ph_front.getString()) + " " + PRED.replaceAll("_", " ") + " " + ph_back.getString().toLowerCase() + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        if (p.length < 2) {
            return getSentStart(ph_front.getString(p[0])) + " does not " + IDENTIFIER_PRED.replace("_", " ")  + " " + getPhBack(p) + ".";
        }
        return getSentStart(ph_front.getString(p[0])) + " does not " + IDENTIFIER_PRED.replace("_", " ") + " " + ph_back.getString(p[1]).toLowerCase() + ".";
    }

    @Override
    public String getNegatedSentence() {
        return getSentStart(ph_front.getString()) + " does not " + IDENTIFIER_PRED.replace("_", " ") + " " + ph_back.getString().toLowerCase() + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_PRED;
    }

    @Override
    public String getIdentifier() {
        String id = "[" + IDENTIFIER_PRED;
        id += "[" + ph_front.getIdentifierString() + "," + ph_back.getIdentifierString() + "]";
        return id + "]";
    }

    private String getPhBack(String... p) {
        if (ph_front.getType() == Placeholder.Type.CONSTANT) {
            return ph_back.getString(p[0]).toLowerCase();
        }
        return ph_back.getString(p[1]).toLowerCase();
    }

    @Override
    public String toString() {return getIdentifier(); }
}
