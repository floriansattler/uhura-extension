package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * ART? [C | Var | Pred Var] [TOBE | DOESNOT]? ART? Pred PH? ART? [C | Var | Pred Var]
 */
public class Template2 extends SentenceTemplate {

    private final Placeholder ph_front;
    private final String ART;
    private final String TOBEDOES;
    private final String ART1;
    private final String PRED;
    private final String IDENTIFIER_ATOM;
    private final String PH;
    private final String ART2;
    private final Placeholder ph_back;

    private final boolean THIRDPERSON;

    private final int SCORE;

    public Template2(Placeholder ph_front, String ART, String TOBEDOES, String ART1, String PRED, String IDENTIFIER_ATOM, String PH, String ART2, Placeholder ph_back) {
        this.ph_front = ph_front;
        this.ART = setArt(convert(ART), ph_front.getType());
        this.TOBEDOES = convertTobeDoes(convert(TOBEDOES));
        this.THIRDPERSON = TOBEDOES.trim().equals("does");
        this.ART1 = setArt(convert(ART1), Placeholder.Type.PRED_VAR);
        this.PRED = PRED;
        this.IDENTIFIER_ATOM = IDENTIFIER_ATOM;
        this.PH = convert(PH);
        this.ART2 = setArt(convert(ART2), ph_back.getType());
        this.ph_back = ph_back;
        this.SCORE = generateScore();
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    private int generateScore() {
        int s = 1;
        switch (ph_front.getType()) {
            case CONSTANT:
                s += 2;
                break;
            case PRED_VAR:
                s += 1;
                break;
        }
        switch (ph_back.getType()) {
            case CONSTANT:
                s += 2;
                break;
            case PRED_VAR:
                s += 1;
                break;
        }
        return s;
    }

    @Override
    public String getSentence(String... p) {
        if (p.length < 2) {
            return getSentStart(ART) + getAtomatStart(ph_front.getString(p[0]), ART) + " " + (THIRDPERSON ? "" : TOBEDOES)
                    + ART1 + (THIRDPERSON ? getThirdPerson(PRED.replaceAll("_", " ")) : PRED.replaceAll("_", " ")) + " " + PH + ART2 + getPhBack(p) + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph_front.getString(p[0]), ART) + " " + (THIRDPERSON ? "" : TOBEDOES)
                + ART1 + (THIRDPERSON ? getThirdPerson(PRED.replaceAll("_", " ")) : PRED.replaceAll("_", " ")) + " " + PH + ART2 + ph_back.getString(p[1]) + ".";
    }

    @Override
    public String getSentence() {
        return getSentStart(ART) + getAtomatStart(ph_front.getString(), ART) + " " + (THIRDPERSON ? "" : TOBEDOES)
                + ART1 + (THIRDPERSON ? getThirdPerson(PRED.replaceAll("_", " ")) : PRED.replaceAll("_", " ")) + " " + PH + ART2 + ph_back.getString() + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph_front.getString(p[0]), ART) + " " + "does not "
                    + ART1 + IDENTIFIER_ATOM.replace("_", " ") + " " + PH + ART2 + getPhBack(p) + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph_front.getString(p[0]), ART) + " " + TOBEDOES + "not "
                + ART1 + PRED.replaceAll("_", " ") + " " + PH + ART2 + getPhBack(p) + ".";
    }

    private String getPhBack(String... p) {
        if (ph_front.getType() == Placeholder.Type.CONSTANT) {
            return ph_back.getString(p[0]);
        }
        return ph_back.getString(p[1]);
    }

    @Override
    public String getNegatedSentence() {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph_front.getString(), ART) + " " + "does not "
                    + ART1 + IDENTIFIER_ATOM.replace("_", " ") + " " + PH + ART2 + ph_back.getString() + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph_front.getString(), ART) + " " + TOBEDOES + "not "
                + ART1 + PRED.replaceAll("_", " ") + " " + PH + ART2 + ph_back.getString() + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_ATOM;
    }

    @Override
    public String getIdentifier() {
        String id = "[" + IDENTIFIER_ATOM;
        id += "[" + ph_front.getIdentifierString() + "," + ph_back.getIdentifierString() + "]]";
        return id;
    }

    @Override
    public String toString() {return getIdentifier(); }
}
