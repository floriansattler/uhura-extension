package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * ART? [C | Var | Pred Var] [TOBE | "does not"]? ART? Pred ART? Pred PH? ART? C
 */
public class Template3 extends SentenceTemplate {

    private final String ART;
    private final Placeholder ph;
    private final String TOBEDOES;
    private final String ART1;
    private final String PRED;
    private final String IDENTIFIER_ATOM;
    private final String ART2;
    private final String PRED1;
    private final String IDENTIFIER_ATOM1;
    private final String PH;
    private final String ART3;
    private final String C;

    private final int SCORE;

    public Template3(String ART, Placeholder ph, String TOBEDOES, String ART1, String PRED, String IDENTIFIER_ATOM, String ART2, String PRED1, String IDENTIFIER_ATOM1, String PH, String ART3, String c) {
        this.ART = setArt(convert(ART), ph.getType());
        this.ph = ph;
        this.TOBEDOES = convertTobeDoes(convert(TOBEDOES));
        this.ART1 = setArt(convert(ART1), Placeholder.Type.PRED_VAR);
        this.PRED = PRED;
        this.IDENTIFIER_ATOM = IDENTIFIER_ATOM;
        this.ART2 = convert(ART2);
        this.PRED1 = PRED1;
        this.PH = convert(PH);
        this.ART3 = convert(ART3);
        this.C = c;
        this.IDENTIFIER_ATOM1 = IDENTIFIER_ATOM1;
        this.SCORE = generateScore();
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    private int generateScore() {
        int s = 4;
        switch (ph.getType()) {
            case PRED_VAR:
                s += 1;
                break;
            case CONSTANT:
                s += 2;
                break;
        }
        return s;
    }

    @Override
    public String getSentence(String... p) {
        return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " " + TOBEDOES
                + ART1 + PRED.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
    }

    @Override
    public String getSentence() {
        return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " " + TOBEDOES
                + ART1 + PRED.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " " + "does not "
                    + ART1 + IDENTIFIER_ATOM.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " " + TOBEDOES + "not "
                + ART1 + PRED.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
    }

    @Override
    public String getNegatedSentence() {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " " + "does not "
                    + ART1 + IDENTIFIER_ATOM.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " " + TOBEDOES + "not "
                + ART1 + PRED.replaceAll("_", " ") + " " + ART2 + PRED1 + " " + PH + ART3 + C + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_ATOM;
    }

    @Override
    public String getIdentifier() {
        String id = "[" + IDENTIFIER_ATOM;
        id += "[" + ph.getIdentifierString() + "," + IDENTIFIER_ATOM1 + "$" + C + "]" + "]";
        return id;
    }

    @Override
    public String toString() {return getIdentifier(); }
}
