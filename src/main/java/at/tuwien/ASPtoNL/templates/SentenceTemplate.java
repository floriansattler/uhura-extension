package at.tuwien.ASPtoNL.templates;

public abstract class SentenceTemplate implements ISentenceTemplate {

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ISentenceTemplate)) {
            return false;
        }
        ISentenceTemplate o = (ISentenceTemplate) obj;
        return o.getIdentifier().equals(this.getIdentifier());
    }

    @Override
    public int hashCode() {
        return this.getIdentifier().hashCode();
    }
}
