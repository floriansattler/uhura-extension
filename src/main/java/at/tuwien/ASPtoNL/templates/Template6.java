package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * Pred "normally" TOBE? [Pred | Pred C]
 */
public class Template6 extends SentenceTemplate {

    private final String PRED;
    private final String IDENTIFIER_PRED;
    private final String TOBE;
    private final String PRED1;
    private final String IDENTIFIER_PRED1;
    private final String C;

    private final int SCORE;

    public Template6(String PRED, String IDENTIFIER_PRED, String TOBE, String PRED1, String IDENTIFIER_PRED1, String c) {
        this.PRED = PRED;
        this.IDENTIFIER_PRED = IDENTIFIER_PRED;
        this.TOBE = convertTobeDoes(convert(TOBE));
        this.PRED1 = PRED1;
        this.IDENTIFIER_PRED1 = IDENTIFIER_PRED1;
        C = convert(c);
        this.SCORE = generateScore();
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    private int generateScore() {
        int s = 1;
        if (!IDENTIFIER_PRED.isEmpty()) {
            s += 1;
        }
        if (!C.isEmpty()) {
            s += 2;
        }
        return s;
    }

    @Override
    public String getSentence(String... p) {
        if (!TOBE.isEmpty()) {
            return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + p[0] + " is " + (!C.trim().isEmpty() ? getIndefiniteArticle() + " " : "") + IDENTIFIER_PRED1.replaceAll("_", " ") + (!C.trim().isEmpty() ? " " + C.trim() : "") +  ".";
        }
        return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + p[0] + " "  + getThirdPerson(PRED1) + ".";
    }

    @Override
    public String getSentence() {
        if (!TOBE.isEmpty()) {
            return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + "X" + " is "+ (!C.trim().isEmpty() ? getIndefiniteArticle() + " " : "")  + PRED1.replaceAll("_", " ") + (!C.trim().isEmpty() ? " " + C.trim() : "") +  ".";
        }
        return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + "X" + " "  + getThirdPerson(PRED1) + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        if (!TOBE.isEmpty()) {
            return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + p[0] + " is not "+ (!C.trim().isEmpty() ? getIndefiniteArticle() + " " : "")  + IDENTIFIER_PRED1.replaceAll("_", " ") + (!C.trim().isEmpty() ? " " + C.trim() : "") + ".";
        }
        return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + p[0] + " does not " + IDENTIFIER_PRED1.replace("_", " ") + ".";
    }

    @Override
    public String getNegatedSentence() {
        if (!TOBE.isEmpty()) {
            return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + "X" + " is not "+ (!C.trim().isEmpty() ? getIndefiniteArticle() + " " : "")  + IDENTIFIER_PRED1.replaceAll("_", " ") + (!C.trim().isEmpty() ? " " + C.trim() : "") + ".";
        }
        return getSentStart(IDENTIFIER_PRED) + (IDENTIFIER_PRED.isEmpty() ? "" : " ") + "X" + " does not " + IDENTIFIER_PRED1.replace("_", " ") + ".";
    }

    private String getIndefiniteArticle() {
        Character c = IDENTIFIER_PRED1.toLowerCase().charAt(0);
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return "an";
            default:
                return "a";
        }
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_PRED1;
    }

    @Override
    public String getIdentifier() {
        if (!C.trim().isEmpty()) {
            return "[" + IDENTIFIER_PRED1 + "[" + (IDENTIFIER_PRED.isEmpty() ? "X" : IDENTIFIER_PRED + "$" + "X") + ","
                    + (C.trim()) + "]]";
        }
        return "[" + IDENTIFIER_PRED1 + "[" + (IDENTIFIER_PRED.isEmpty() ? "X" : IDENTIFIER_PRED + "$X") + "]]";
    }

    @Override
    public String toString() {return getIdentifier(); }
}
