package at.tuwien.ASPtoNL.templates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.getSentStart;

public class DefaultTemplate extends SentenceTemplate {

    private final String PAT = "([A-Z] | [A-Z] | [A-Z]\\.)";
    private Pattern pattern;

    private int variableCnt = 0;

    private String normalTemp, negatedTemp;
    private String identifierPred;


    public DefaultTemplate(String normalTemp, String negatedTemp, String identifierPred) {
        pattern = Pattern.compile(PAT);
        Matcher matcher = pattern.matcher(normalTemp);

        while (matcher.find()) {
            variableCnt++;
        }

        this.normalTemp = normalTemp;
        this.negatedTemp = negatedTemp;

        this.identifierPred = identifierPred;
    }

    @Override
    public String getSentence(String... p) {
        String s = normalTemp;
        if (p.length != variableCnt) {
            return s;
        }
        s = s.replaceFirst(PAT, getSentStart(p[0]) + " ");
        for (int i = 1; i < variableCnt; i++) {
            s = s.replaceFirst(PAT, " " + p[i]);
        }
        return (s.endsWith(".") ? s : s + ".");
    }

    @Override
    public String getSentence() {
        return normalTemp;
    }

    @Override
    public String getNegatedSentence(String... p) {
        String s = negatedTemp;
        if (p.length != variableCnt) {
            return s;
        }
        s = s.replaceFirst(PAT, getSentStart(p[0]) + " ");
        for (int i = 1; i < variableCnt; i++) {
            s = s.replaceFirst(PAT, " " + p[i]);
        }
        return (s.endsWith(".") ? s : s + ".");
    }

    @Override
    public String getNegatedSentence() {
        return "";
    }

    @Override
    public String getMainIdentifier() {
        return identifierPred;
    }

    @Override
    public String getIdentifier() {
        String s = "[" + identifierPred + "[X";
        for (int i = 1; i < variableCnt; i++) {
            s += ",X";
        }
        return s +"]]";
    }

    @Override
    public Integer getScore() {
        return 1;
    }

    @Override
    public String toString() {return getIdentifier(); }
}
