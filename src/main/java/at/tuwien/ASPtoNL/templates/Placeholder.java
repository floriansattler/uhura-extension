package at.tuwien.ASPtoNL.templates;

public class Placeholder {

    private Type type;
    private String ph_string;
    private String ph_string_identifier;

    public Placeholder(String pred, String var, String identifier_pred) {
        type = Type.PRED_VAR;
        ph_string = pred;
        ph_string_identifier = identifier_pred.toLowerCase();
    }

    public Placeholder(String const_var) {
        if (const_var.length() == 1) {
            type = Type.VARIABLE;
            ph_string = "X";
            ph_string_identifier = "X";
        } else {
            type = Type.CONSTANT;
            ph_string = const_var;
            ph_string_identifier = const_var.toLowerCase();
        }
    }

    public String getString(String x) {
        if (type == Type.PRED_VAR) {
            return ph_string + " " + x;
        }
        if (type == Type.VARIABLE) {
            return x;
        }
        return ph_string.replaceAll("_", " ");
    }

    public String getString() {
        return ph_string;
    }

    public String getIdentifierString() {
        if (type == Type.PRED_VAR) {
            return ph_string_identifier + "$X";
        }
        if (type == Type.VARIABLE) {
            return "X";
        }
        return ph_string_identifier;
    }

    public Type getType() {
        return type;
    }

    public enum Type{
        CONSTANT,
        VARIABLE,
        PRED_VAR
    }
}
