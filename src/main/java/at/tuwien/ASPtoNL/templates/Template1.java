package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * ART? [C | Var | Pred Var] [TOBE | "does not"]? ART1? Pred
 */
public class Template1 extends SentenceTemplate {

    private final Placeholder ph;
    private final String ART;
    private final String TOBEDOES;
    private final String ART1;
    private final String PRED;
    private final String IDENTIFIER_PRED;

    private final boolean THIRDPERSON;

    private final int SCORE;

    public Template1(Placeholder ph, String ART, String TOBEDOES, String ART1, String PRED, String IDENTIFIER_PRED) {
        this.ph = ph;
        this.ART = setArt(convert(ART), ph.getType());
        this.TOBEDOES = convertTobeDoes(convert(TOBEDOES));
        this.ART1 = convert(ART1);
        this.THIRDPERSON = TOBEDOES.trim().equals("does");
        this.PRED = PRED;
        this.IDENTIFIER_PRED = IDENTIFIER_PRED;
        this.SCORE = generateScore();
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    private int generateScore() {
        int s = 1;
        switch (ph.getType()) {
            case PRED_VAR:
                s += 1;
                break;
            case CONSTANT:
                s += 2;
                break;
        }
        return s;
    }

    @Override
    public String getSentence() {
        if (THIRDPERSON) {
            return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " "
                    + getThirdPerson(PRED.replaceAll("_", " ")) + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " "
                + TOBEDOES + ART1 + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getNegatedSentence() {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " "
                    + "does not " + ART1 + IDENTIFIER_PRED.replaceAll("_", " ") + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(), ART) + " "
                + TOBEDOES + "not " + ART1 + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getSentence(String... p) {
        if (THIRDPERSON) {
            return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " "
                    + getThirdPerson(PRED.replaceAll("_", " ")) + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " "
                + TOBEDOES + ART1 + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        if (TOBEDOES.isEmpty()) {
            return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " "
                    + "does not " + ART1 + IDENTIFIER_PRED.replaceAll("_", " ") + ".";
        }
        return getSentStart(ART) + getAtomatStart(ph.getString(p[0]), ART) + " "
                + TOBEDOES + "not " + ART1 + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_PRED;
    }

    @Override
    public String getIdentifier() {
        String id = "[" + IDENTIFIER_PRED;
        id += "[" + ph.getIdentifierString() + "]";
        id += "]";
        return id;
    }

    @Override
    public String toString() {
        return this.getIdentifier();
    }
}
