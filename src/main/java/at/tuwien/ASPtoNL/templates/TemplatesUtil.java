package at.tuwien.ASPtoNL.templates;

public final class TemplatesUtil {

    public static String convert(String s) {
        return (s == null || s.isEmpty() ? "" : s + " ");
    }

    public static String getSentStart(String s) {
        if (s.isEmpty()) {
            return "";
        }
        Character c = s.charAt(0);
        c = Character.toUpperCase(c);
        s = s.substring(1);
        return c + s;
    }

    public static String getThirdPerson(String verb) {
        String back = null;
        if (verb.contains(" ")) {
            back = verb.substring(verb.indexOf(" ")+1);
            verb = verb.substring(0, verb.indexOf(" "));
        } else if (verb.contains("_")) {
            back = verb.substring(verb.indexOf("_")+1);
            verb = verb.substring(0, verb.indexOf("_"));
        }
        switch (verb) {
            case "do":
                return "does";
            case "go":
                return "goes";
            case "have":
                return "has";
            case "life":
                return "lives";
            case "say":
                return "says";
        }
        //-ch, -s, -sh, -x, -z
        if (verb.endsWith("ch") ||
                verb.endsWith("s") ||
                verb.endsWith("sh") ||
                verb.endsWith("x") ||
                verb.endsWith("z") || verb.endsWith("do") || verb.endsWith("go")) {
            return verb + "es";
        }
        if (verb.endsWith("y")) {
            return verb.substring(0, verb.length()-1) + "ies";
        }
        return verb + "s" + (back != null ? " " + back : "");
    }

    public static String getAtomatStart(String atom, String art) {
        if (art.isEmpty()) {
            return TemplatesUtil.getSentStart(atom);
        }
        return atom;
    }

    public static String setArt(String art, Placeholder.Type type) {
        if (art.isEmpty()) {
            return "";
        } else if (type == Placeholder.Type.PRED_VAR) {
            return "the ";
        }
        return "";
    }

    public static String convertTobeDoes(String tobedoes) {
        if (tobedoes.contains("not") || tobedoes.contains("n't")) {
            switch (tobedoes) {
                case "is not ":
                case "are not ":
                case "isn't ":
                case "aren't ":
                    return "is ";

                case "does not ":
                case "doesn't ":
                    return "does ";
            }
        }
        return tobedoes;
    }
}
