package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * "There" TOBE ART? Pred VAR
 */
public class Template4 extends SentenceTemplate {

    private final String TOBE;
    private final String ART;
    private final String PRED;
    private final String IDENTIFIER_PRED;

    private final int SCORE;

    public Template4(String TOBE, String ART, String PRED, String IDENTIFIER_PRED) {
        this.TOBE = convertTobeDoes(convert(TOBE));
        this.ART = convert(ART);
        this.PRED = PRED;
        this.IDENTIFIER_PRED = IDENTIFIER_PRED;
        this.SCORE = 1;
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    @Override
    public String getSentence(String... p) {
        return getSentStart(p[0]) + (!TOBE.isEmpty() ? " is " : "") + ART + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getSentence() {
        return "X " + (!TOBE.isEmpty() ? " is " : "") + ART + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        return getSentStart(p[0]) + (!TOBE.isEmpty() ? " is " : "") + "not " + ART + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getNegatedSentence() {
        return "X " + (!TOBE.isEmpty() ? " is " : "") + "not " + ART + PRED.replaceAll("_", " ") + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_PRED;
    }

    @Override
    public String getIdentifier() {
        return "[" + IDENTIFIER_PRED + "[X]]";
    }

    @Override
    public String toString() {return getIdentifier(); }
}
