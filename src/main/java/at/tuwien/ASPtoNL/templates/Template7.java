package at.tuwien.ASPtoNL.templates;

import static at.tuwien.ASPtoNL.templates.TemplatesUtil.*;

/**
 * This class stores the sentence with the following pattern:
 * QUA Pred TOBE Pred
 */
public class Template7 extends SentenceTemplate {

    private final String PRED;
    private final String IDENTIFIER_PRED;
    private final String TOBE;
    private final String PRED1;
    private final String IDENTIFIER_PRED1;

    private final int SCORE;

    public Template7(String PRED, String IDENTIFIER_PRED, String TOBE, String PRED1, String IDENTIFIER_PRED1) {
        this.PRED = PRED;
        this.IDENTIFIER_PRED = IDENTIFIER_PRED;
        this.TOBE = convertTobeDoes(convert(TOBE));
        this.PRED1 = PRED1;
        this.IDENTIFIER_PRED1 = IDENTIFIER_PRED1;
        this.SCORE = 2;
    }

    @Override
    public Integer getScore() {
        return SCORE;
    }

    @Override
    public String getSentence(String... p) {
        return getSentStart(IDENTIFIER_PRED) + " " + p[0] + " is " + IDENTIFIER_PRED1 + ".";
    }

    @Override
    public String getSentence() {
        return getSentStart(IDENTIFIER_PRED) + " X is " + IDENTIFIER_PRED1 + ".";
    }

    @Override
    public String getNegatedSentence(String... p) {
        return getSentStart(IDENTIFIER_PRED) + " " + p[0] + " is not " + IDENTIFIER_PRED1 + ".";
    }

    @Override
    public String getNegatedSentence() {
        return getSentStart(IDENTIFIER_PRED) + " X is " + IDENTIFIER_PRED1 + ".";
    }

    @Override
    public String getMainIdentifier() {
        return IDENTIFIER_PRED1;
    }

    @Override
    public String getIdentifier() {
        return "[" + IDENTIFIER_PRED1 + "[" + IDENTIFIER_PRED + "$X]]";
    }

    @Override
    public String toString() {return getIdentifier(); }
}
