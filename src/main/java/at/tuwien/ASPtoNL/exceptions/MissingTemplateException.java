package at.tuwien.ASPtoNL.exceptions;

import java.util.List;

public class MissingTemplateException extends Exception {

    private List<String> missingTemplates;

    public MissingTemplateException(List<String> missingTemplates) {
        this.missingTemplates = missingTemplates;
    }

    public List<String> getMissingTemplates() {
        return missingTemplates;
    }
}
