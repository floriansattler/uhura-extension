package at.tuwien.ASPtoNL;

import at.tuwien.ASPtoNL.templates.*;
import generatedANTLR4.CNLListener;
import generatedANTLR4.CNLParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashSet;
import java.util.Set;


public class CNLWalker implements CNLListener {

    private Set<String> atoms;

    private Set<ISentenceTemplate> template_set;

    public CNLWalker(Set<String> atoms) {
        this.atoms = atoms;
        template_set = new HashSet<>();
    }

    private String convert(TerminalNode n) {
        return (n == null ? "" : n.getText());
    }

    private String tobe(TerminalNode tobe) {
        if (tobe != null) {
            return tobe.getText();
        }
        return "";
    }

    private String tobeDoes(TerminalNode tobe, TerminalNode doesnot) {
        if (tobe != null) {
            return tobe(tobe);
        } else if (doesnot != null) {
            return doesnot.getText();
        }
        return "";
    }

    /**
     * This method converts the cnl predicate form into the answer set predicate form.
     * @param w is the string which contains the predicate in the cnl form
     * @return the answer set form of the predicate
     */
    private String getASForm(String w) {
        boolean twoPart = false;
        String secPart = "";
        w = w.toLowerCase();

        if (w.contains("_")) {
            twoPart = true;
            if (w.startsWith("live")) {
                w = w.replace("live", "life");
            }
        }

        if (atoms.contains(w)) {
            return w;
        } else if (twoPart && atoms.contains(w)) {
            return w;
        }

        if (w.equals("has") && atoms.contains("have")) {
            return "have";
        }
        if (twoPart) {
            String h = w;
            w = w.substring(0, w.indexOf("_"));
            secPart = h.substring(h.indexOf("_")+1);
        }
        int currDiff = Integer.MAX_VALUE;
        String predicate = "";
        for (String a :
                atoms) {
            if (twoPart) {
                if (!a.contains("_")) {
                    continue;
                } else if (!a.substring(a.indexOf("_") + 1).equals(secPart)) {
                    continue;
                }
                a = a.substring(0, a.indexOf("_"));
            }
            String h = a;
            if (a.endsWith("y")) {
                h = a.substring(0, a.length()-1);
            } else if (a.endsWith("ie")) {
                h = a.substring(0,a.length()-2);
            }
            if (w.startsWith(h)) {
                int diff = w.length() - a.length();
                if (diff > 0) {
                    currDiff = Math.min(currDiff, diff);
                }
                predicate = (diff == currDiff ? (twoPart ? a + "_" + secPart : a) : predicate);
            }
        }
        return predicate;
    }

    public Set<ISentenceTemplate> getTemplates(){
        return template_set;
    }

    @Override
    public void enterText(CNLParser.TextContext ctx) {

    }

    @Override
    public void exitText(CNLParser.TextContext ctx) {

    }

    @Override
    public void enterSentence_types(CNLParser.Sentence_typesContext ctx) {

    }

    @Override
    public void exitSentence_types(CNLParser.Sentence_typesContext ctx) {

    }

    @Override
    public void enterPattern1(CNLParser.Pattern1Context ctx) {
        Template1 simpleVar;
        Template1 template1;

        Placeholder var;
        Placeholder ph = null;
        if (ctx.ph() != null) {
            if (ctx.ph().C() != null) {
                ph = new Placeholder(ctx.ph().C().getText());
            } else if (ctx.ph().VAR() != null) {
                ph = new Placeholder(ctx.ph().VAR().getText());
            } else if (ctx.ph().predvar() != null) {
                ph = new Placeholder(ctx.ph().predvar().PRED().getText(), ctx.ph().predvar().VAR().getText(), getASForm(convert(ctx.ph().predvar().PRED())));
            }
        }

        if (ph.getType() != Placeholder.Type.VARIABLE) {
            var = new Placeholder("X");
            simpleVar = new Template1(var, convert(ctx.ph().ART()),
                    tobeDoes(ctx.TOBE(), ctx.DOESNOT()), convert(ctx.ART()), convert(ctx.PRED()), getASForm(convert(ctx.PRED())));
            this.template_set.add(simpleVar);
        }

        template1 = new Template1(ph, convert(ctx.ph().ART()), tobeDoes(ctx.TOBE(), ctx.DOESNOT()), convert(ctx.ART()), convert(ctx.PRED()), getASForm(convert(ctx.PRED())));
        this.template_set.add(template1);
    }

    @Override
    public void enterPattern2(CNLParser.Pattern2Context ctx) {
        Template2 template2;
        Template2 complex1Template1Var;

        Placeholder ph_front = null, ph_back = null;
        Placeholder var;
        if (ctx.ph(0) != null) {
            CNLParser.PhContext ph = ctx.ph(0);
            //fill in ph_front
            if (ph.C() != null) {
                ph_front = new Placeholder(ph.C().getText());
            } else if (ph.VAR() != null) {
                ph_front = new Placeholder(ph.VAR().getText());
            } else if (ph.predvar() != null) {
                ph_front = new Placeholder(ph.predvar().PRED().getText(), ph.predvar().VAR().getText(), getASForm(convert(ph.predvar().PRED())));
            }
        }
        if (ctx.ph(1) != null) {
            CNLParser.PhContext ph = ctx.ph(1);
            //fill in ph_front
            if (ph.C() != null) {
                ph_back = new Placeholder(ph.C().getText());
            } else if (ph.VAR() != null) {
                ph_back = new Placeholder(ph.VAR().getText());
            } else if (ph.predvar() != null) {
                ph_back = new Placeholder(ph.predvar().PRED().getText(), ph.predvar().VAR().getText(), getASForm(convert(ph.predvar().PRED())));
            }
        }

        if ((ph_front != null && ph_back != null)  &&
        ph_front.getType() != Placeholder.Type.VARIABLE && ph_back.getType() != Placeholder.Type.VARIABLE) {
            var = new Placeholder("X");
            complex1Template1Var = new Template2(var, "", tobeDoes(ctx.TOBE(), ctx.DOESNOT()),
                    convert(ctx.ART()), convert(ctx.PRED()), getASForm(convert(ctx.PRED())),
                    convert(ctx.PH()), "", var);
            this.template_set.add(complex1Template1Var);
        }

        template2 = new Template2(ph_front, convert(ctx.ph(0).ART()), tobeDoes(ctx.TOBE(), ctx.DOESNOT()),
                convert(ctx.ART()), convert(ctx.PRED()), getASForm(convert(ctx.PRED())),
                convert(ctx.PH()), convert(ctx.ph(1).ART()), ph_back);
        this.template_set.add(template2);
    }

    @Override
    public void enterPattern3(CNLParser.Pattern3Context ctx) {
        Template3 template3;
        Template3 template3Var;

        Placeholder ph = null;
        Placeholder var;
        if (ctx.ph() != null) {
            if (ctx.ph().C() != null) {
                ph = new Placeholder(ctx.ph().C().getText());
            } else if (ctx.ph().VAR() != null) {
                ph = new Placeholder(ctx.ph().VAR().getText());
            } else if (ctx.ph().predvar() != null) {
                ph = new Placeholder(ctx.ph().predvar().PRED().getText(), ctx.ph().predvar().VAR().getText(), getASForm(convert(ctx.ph().predvar().PRED())));
            }
        }

        if (ph != null && ph.getType() != Placeholder.Type.VARIABLE) {
            var = new Placeholder("X");
            template3Var = new Template3(convert(ctx.ph().ART()),
                    var,
                    tobeDoes(ctx.TOBE(), ctx.DOESNOT()),
                    convert(ctx.ART()),
                    convert(ctx.PRED()),
                    getASForm(convert(ctx.PRED())),
                    convert(ctx.artpred().ART()),
                    convert(ctx.artpred().PRED()),
                    getASForm(convert(ctx.artpred().PRED())),
                    convert(ctx.PH()),
                    convert(ctx.artconst().ART()),
                    convert(ctx.artconst().C())
                    );
            template_set.add(template3Var);
        }

        template3 = new Template3(convert(ctx.ph().ART()),
                ph,
                tobeDoes(ctx.TOBE(), ctx.DOESNOT()),
                convert(ctx.ART()),
                convert(ctx.PRED()),
                getASForm(convert(ctx.PRED())),
                convert(ctx.artpred().ART()),
                convert(ctx.artpred().PRED()),
                getASForm(convert(ctx.artpred().PRED())),
                convert(ctx.PH()),
                convert(ctx.artconst().ART()),
                convert(ctx.artconst().C())
        );
        template_set.add(template3);
    }

    @Override
    public void enterPattern4(CNLParser.Pattern4Context ctx) {
        Template4 template4 = new Template4(
                tobe(ctx.TOBE()),
                convert(ctx.ART()),
                convert(ctx.PRED()),
                getASForm(convert(ctx.PRED()))
        );

        template_set.add(template4);
    }

    @Override
    public void enterPattern5(CNLParser.Pattern5Context ctx) {
        Template5 template5;
        Template5 template5Var;

        Placeholder ph_front = null, ph_back = null;
        Placeholder var;

        if (ctx.C() != null) {
            ph_front = new Placeholder(ctx.C().getText());
        } else if (ctx.predvar() != null) {
            ph_front = new Placeholder(ctx.predvar(0).PRED().getText(),
                    ctx.predvar(0).VAR().getText(),
                    getASForm(ctx.predvar(0).PRED().getText()));
        }

        if (ctx.predvar(1) != null) {
            ph_back = new Placeholder(ctx.predvar(1).PRED().getText(),
                    ctx.predvar(1).VAR().getText(),
                    getASForm(ctx.predvar(1).PRED().getText()));
        }

        if (ph_front != null && ph_front.getType() != Placeholder.Type.VARIABLE) {
            var = new Placeholder("X");
            template5Var = new Template5(
                    var,
                    convert(ctx.PRED()),
                    getASForm(convert(ctx.PRED())),
                    ph_back
            );
            template_set.add(template5Var);
        }

        template5 = new Template5(
                ph_front,
                convert(ctx.PRED()),
                getASForm(convert(ctx.PRED())),
                ph_back
        );
        template_set.add(template5);
    }

    @Override
    public void enterPattern6(CNLParser.Pattern6Context ctx) {
        Template6 template6 = new Template6(
                convert(ctx.PRED(0)),
                getASForm(convert(ctx.PRED(0))),
                tobe(ctx.TOBE()),
                convert(ctx.PRED(1)),
                getASForm(convert(ctx.PRED(1))),
                convert(ctx.C())
        );

        Template6 template6Var = new Template6(
                "",
                "",
                tobe(ctx.TOBE()),
                convert(ctx.PRED(1)),
                getASForm(convert(ctx.PRED(1))),
                convert(ctx.C())
        );

        template_set.add(template6);
        template_set.add(template6Var);
    }

    @Override
    public void enterPattern7(CNLParser.Pattern7Context ctx) {
        Template7 template7 = new Template7(
                convert(ctx.PRED(0)),
                getASForm(convert(ctx.PRED(0))),
                tobe(ctx.TOBE()),
                convert(ctx.PRED(1)),
                getASForm(convert(ctx.PRED(1)))
        );
        template_set.add(template7);
    }

    @Override
    public void exitPattern1(CNLParser.Pattern1Context ctx) {

    }

    @Override
    public void exitPattern2(CNLParser.Pattern2Context ctx) {

    }

    @Override
    public void exitPattern3(CNLParser.Pattern3Context ctx) {

    }

    @Override
    public void exitPattern4(CNLParser.Pattern4Context ctx) {

    }

    @Override
    public void exitPattern5(CNLParser.Pattern5Context ctx) {

    }

    @Override
    public void exitPattern6(CNLParser.Pattern6Context ctx) {

    }

    @Override
    public void exitPattern7(CNLParser.Pattern7Context ctx) {

    }

    @Override
    public void enterArtpred(CNLParser.ArtpredContext ctx) {

    }

    @Override
    public void exitArtpred(CNLParser.ArtpredContext ctx) {

    }

    @Override
    public void enterPredvar(CNLParser.PredvarContext ctx) {

    }

    @Override
    public void exitPredvar(CNLParser.PredvarContext ctx) {

    }

    @Override
    public void enterArtconst(CNLParser.ArtconstContext ctx) {

    }

    @Override
    public void exitArtconst(CNLParser.ArtconstContext ctx) {

    }

    @Override
    public void enterSentence(CNLParser.SentenceContext ctx) {

    }

    @Override
    public void exitSentence(CNLParser.SentenceContext ctx) {

    }

    @Override
    public void enterPh(CNLParser.PhContext ctx) {

    }

    @Override
    public void exitPh(CNLParser.PhContext ctx) {

    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {

    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {

    }
}
