package at.tuwien.ASPtoNL;

import at.tuwien.ASPtoNL.exceptions.MissingTemplateException;
import at.tuwien.ASPtoNL.templates.ISentenceTemplate;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NLGenerator {

    private Set<String> ANSWER_SET;

    private Set<String> PREDS;
    private Set<String> CONSTANTS;

    private Set<String> CNLPREDS;
    private Set<String> CNLCONSTANTS;

    private List<String> words;
    private List<String> lc_words;
    private List<String> two_part_predicates = new LinkedList<>();
    private List<String> two_part_constants = new LinkedList<>();
    private String text;

    public NLGenerator(Collection<String> answerset, Collection<String> sentences) {
        text = "";
        for (String s : sentences) {
            text += " " + s;
        }
        this.words = new ArrayList<>();
        this.lc_words = new ArrayList<>();
        for (String s : sentences) {
            for (String w : s.split(" ")) {
                words.add((w.endsWith(".") ? w.substring(0,w.length()-1) : w));
                lc_words.add((w.endsWith(".")) ? w.substring(0,w.length()-1).toLowerCase() : w.toLowerCase());
            }
        }

        if (isEmpty(answerset)) {
            this.ANSWER_SET = new HashSet<>();
            this.CONSTANTS = new HashSet<>();
            this.CNLPREDS = new HashSet<>();
            this.CNLCONSTANTS = new HashSet<>();
            this.words = new LinkedList<>();
            this.lc_words = new LinkedList<>();
            this.two_part_predicates = new LinkedList<>();
            return;
        }

        this.ANSWER_SET = new HashSet<>();
        for (String a : answerset) {
            if (!a.isEmpty()) {
                this.ANSWER_SET.add((a.endsWith(".") ? a.substring(0, a.length()-1) : a));
            }
        }
        this.PREDS = extractPredicateSymbols(this.ANSWER_SET);
        this.CONSTANTS = extractConstantSymbols(this.ANSWER_SET);
        this.CNLCONSTANTS = new HashSet<>();
        this.CNLPREDS = new HashSet<>();
        for (String a : this.PREDS) {
            if (!a.contains("_")) {
                if (words.contains(a)) {
                    this.CNLPREDS.add(a);
                }
                if (words.contains(Character.toUpperCase(a.charAt(0)) + a.substring(1))) {
                    this.CNLPREDS.add(Character.toUpperCase(a.charAt(0)) + a.substring(1));
                }
                Set<String> h = lookForTwoPartPred(a);
                if (h != null) {
                    this.CNLPREDS.addAll(h);
                }
            } else {
                for (String sent : sentences) {
                    if (sent.contains(a.replace('_', ' '))) {
                        this.CNLPREDS.add(a);
                    }
                }
            }
        }
        this.CNLPREDS.addAll(generateCNLPreds(sentences));
        this.CNLCONSTANTS.addAll(generateCNLConstants());
    }

    private boolean isEmpty(Collection<String> as) {
        if (as.isEmpty()) {
            return true;
        }

        for (String s : as) {
            if (!s.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    private Set<String> lookForTwoPartPred(String p) {
        if (lc_words.contains(p)) {
            return null;
        }
        for (int idxstart = 0; idxstart < lc_words.size(); idxstart++) {
            String w = lc_words.get(idxstart);
            if (p.startsWith(w) && p.length() > w.length()) {
                String concatword = w;
                int idx = idxstart;
                while (idx+1 < lc_words.size() && p.startsWith(concatword + lc_words.get(idx + 1))) {
                    concatword += lc_words.get(++idx);
                }
                if (p.equals(concatword)) {
                    concatword = words.get(idxstart);
                    String plural = concatword, twopartpred = concatword, twopartpredplural = concatword;
                    for (int i = idxstart+1; i < idx+1; i++) {
                        concatword +=  words.get(i);
                        plural += (i == idx ? Pluralizer.pluralize(words.get(i)) : words.get(i));
                        twopartpred += " " + words.get(i);
                        twopartpredplural += " " + (i == idx ? Pluralizer.pluralize(words.get(i)) : words.get(i));
                    }
                    two_part_predicates.add(twopartpred);
                    two_part_predicates.add(twopartpredplural);
                    return new HashSet<>(Arrays.asList(concatword, plural));
                }

            }
        }
        return null;
    }

    private Map<String, List<ISentenceTemplate>> getTemplateMap(Set<ISentenceTemplate> gen_templates) {
        Map<String, List<ISentenceTemplate>> temps = new HashMap<>();
        for (ISentenceTemplate t :
                gen_templates) {
            if (!temps.containsKey(t.getMainIdentifier())) {
                temps.put(t.getMainIdentifier(), new LinkedList<ISentenceTemplate>(Arrays.asList(t)));
            } else {
                temps.get(t.getMainIdentifier()).add(t);
            }
        }

        for (Map.Entry<String, List<ISentenceTemplate>> e :
                temps.entrySet()) {
            Collections.sort(e.getValue(), new TemplateComparator());
        }

        return temps;
    }

    private static class TemplateComparator implements Comparator<ISentenceTemplate> {
        @Override
        public int compare(ISentenceTemplate o1, ISentenceTemplate o2) {
            return o2.getScore().compareTo(o1.getScore());
        }
    }

    public List<String> getSentences(Set<ISentenceTemplate> gen_tempaltes, Set<String> ANSWER_SET) throws MissingTemplateException {
        List<String> sents = getSentencesSet(gen_tempaltes, ANSWER_SET);
        Collections.sort(sents);
        return sents;
    }

    private List<String> getSentencesSet(Set<ISentenceTemplate> gen_tempaltes, Set<String> ANSWER_SET) throws MissingTemplateException {
        Map<String, List<ISentenceTemplate>> templateMap = getTemplateMap(gen_tempaltes);
        List<String> text = new LinkedList<>();

        for (String atom : ANSWER_SET) {
            boolean neg = false;
            if (atom.startsWith("-")) {
                atom = atom.substring(1);
                neg = true;
            }

            List<ISentenceTemplate> templateList = templateMap.get(atom.substring(0,atom.indexOf("(")));
            if (templateList == null) {
                List<String> missingAtoms = new LinkedList<>();
                for (String a : ANSWER_SET) {
                    if (a.startsWith("-")) {
                        a = a.substring(1);
                    }

                    templateList = templateMap.get(a.substring(0,a.indexOf("(")));
                    if (templateList == null && !containsPredicate(missingAtoms, a)) {
                        missingAtoms.add(a);
                    }
                }

                throw new MissingTemplateException(missingAtoms);
            }

            boolean foundTempalte = false;
            for (int i = 0; i < templateList.size(); i++) {
                ISentenceTemplate template = templateList.get(i);

                if (matches(template.getIdentifier(), atom)) {
                    if (neg) {
                        text.add(template.getNegatedSentence(retrieveConstants(atom)));
                    } else {
                        text.add(template.getSentence(retrieveConstants(atom)));
                    }
                    foundTempalte = true;
                    break;
                }
            }

            if (!foundTempalte) {
                List<String> missingAtom = new LinkedList<>();
                missingAtom.add(atom);
                throw new MissingTemplateException(missingAtom);
            }
        }
        return text;
    }

    private boolean containsPredicate(List<String> preds, String p) {
        for (String s : preds) {
            if (s.substring(0,s.indexOf("(")).equals(p.substring(0,p.indexOf("(")))) {
                return true;
            }
        }
        return false;
    }

    public boolean matches(String identifier, String atom) {
        String[] constants = retrieveConstants(atom);
        String[] fillers = retrieveFillers(identifier);

        if (constants.length != fillers.length) {
            return false;
        }

        for (int i = 0; i < constants.length; i++) {
            if (!compareConstFiller(fillers[i], constants[i])){
                return false;
            }
        }

        return true;
    }

    private boolean compareConstFiller(String filler, String constant) {
        if (filler.contains("$")) {
            //CASE PRED VAR or PRED C
            if (getVarOrConst(filler).length() == 1) {
                //CASE PRED VAR
                return this.ANSWER_SET.contains(getAtom(filler) + "(" + constant.toLowerCase() + ")");
            } else {
                //CASE PRED CONST
                return getVarOrConst(filler).replaceAll("_", "").equals(constant.toLowerCase().replaceAll("_", ""));
            }
        } else {
            //CASE CONSTANT or VAR
            if (filler.length() == 1) {
                //CASE VAR
                return true;
            } else {
                return filler.replaceAll("_", "").equals(constant.toLowerCase());
            }
        }
    }

    private String getAtom(String filler) {
        return filler.substring(0, filler.indexOf("$"));
    }

    private String getVarOrConst(String filler) {
        return filler.substring(filler.indexOf("$") + 1);
    }

    private String[] retrieveConstants(String atom) {
        atom = atom.substring(atom.indexOf('(')+1);
        atom = atom.substring(0,atom.length()-1);
        String[] constansymbols = atom.split(",");
        for (int i = 0; i < constansymbols.length; i++) {
            for (String cnlc : this.CNLCONSTANTS) {
                if (cnlc.contains("_") && cnlc.replaceAll("_", "").toLowerCase().equals(constansymbols[i])) {
                    constansymbols[i] = cnlc.replaceAll("_", " ");
                } else if (cnlc.toLowerCase().equals(constansymbols[i])) {
                    constansymbols[i] = cnlc;
                }
            }
        }
        return constansymbols;
    }

    private String[] retrieveFillers(String identifier) {
        identifier = identifier.substring(1);
        identifier = identifier.substring(identifier.indexOf('[')+1);
        identifier = identifier.substring(0,identifier.length()-2);
        return identifier.split(",");
    }

    /**
     * In the first step the present participle form is generated and if this form occurs as word in the text
     * the word is added to the set of atoms which are later integrated in the grammar. In the last step the generateThirdPersonAndPlural method is called to handle
     * the third person verb occurrences or noun plural forms.
     * @param sentences of the input text
     * @return a set of possible candidates for atoms which may occur in the text
     */
    private Set<String> generateCNLPreds(Collection<String> sentences) {
        Set<String> add_preds = new HashSet<>();

        for (String a : this.PREDS) {
            String h = a;
            if (a.endsWith("ic")) {
                h += "king";
            } else if (a.endsWith("ie")) {
                h = h.substring(0, h.length()-2) + "ying";
            } else if (a.endsWith("e")) {
                h = h.substring(0,h.length()-1) + "ing";
            } else {
                h += h.charAt(h.length()-1) + "ing";
            }
            if (words.contains(h)) {
                add_preds.add(h);
            }
            if (words.contains(a + "ing")) {
                add_preds.add(a + "ing");
            }
        }

        add_preds.addAll(generateThirdPersonAndPlural(sentences));

        return add_preds;
    }

    /**
     * This method retrieves the third person verbs and plural nouns of the text.
     * At first we search for each atom with the help of a regular expression.
     * The regular expression takes all but the last character and matches with all appendices which are built with the letters "ies".
     * The matches are added to the set of atoms for the grammar.
     * @param sentences of the input text
     * @return a set of possible candidates (third person verb or plural noun) for atoms which may occur in the text
     */
    private Set<String> generateThirdPersonAndPlural(Collection<String> sentences) {
        Set<String> gen_preds = new HashSet<>();

        for (String a : this.PREDS) {
            String h = a;
            if (a.contains("_")) {
                a = a.substring(0,a.indexOf("_"));
            }

            if (a.equals("life")) {
                gen_preds.add("lives" + h.substring(h.indexOf("_")));
                continue;
            }

            String pat = a.substring(0, a.length() - 1);
            pat += "[" + a.charAt(a.length()-1) + "]?";
            pat += "[ies]+[' '|'.']";

            Pattern pattern = Pattern.compile(pat);
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                String s = matcher.group(0).trim();
                s = (s.endsWith(".") ? s.substring(0,s.length()-1) : s);

                if (s.equals(a)) {
                    continue;
                }

                if (h.contains("_")) {
                    gen_preds.add(s + h.substring(h.indexOf("_")));
                } else {
                    gen_preds.add(s);
                    gen_preds.add(Character.toUpperCase(s.charAt(0)) + s.substring(1));
                }
            }

            pat = Character.toUpperCase(a.charAt(0)) + a.substring(1, a.length() - 1);
            pat += "[" + a.charAt(a.length()-1) + "]?";
            pat += "[ies]+[' '|'.']";

            pattern = Pattern.compile(pat);
            matcher = pattern.matcher(text);

            while (matcher.find()) {
                String s = matcher.group(0).trim();
                s = (s.endsWith(".") ? s.substring(0,s.length()-1) : s);

                if (h.contains("_")) {
                    gen_preds.add(s + h.substring(h.indexOf("_")));
                } else {
                    gen_preds.add(s);
                    gen_preds.add(Character.toUpperCase(s.charAt(0)) + s.substring(1));
                }
            }
        }

        return gen_preds;
    }

    public String preprocessCNLText(String text) {
        for (String pred : this.CNLPREDS) {
            if (pred.contains("_")) {
                text = text.replaceAll(pred.replace('_', ' '), pred);
            }
        }

        for (String constant : this.CNLCONSTANTS) {
            if (constant.contains("_")) {
                text = text.replaceAll(constant.replace('_', ' '), constant);
            }
        }

        for (String tpp : this.two_part_predicates) {
            text = text.replaceAll(tpp, tpp.replace(" ", ""));
        }

        return text;
    }

    private Set<String> generateCNLConstants() {
        Set<String> upperCaseConstants = new HashSet<>();
        for (String c : this.CONSTANTS) {
            if (lc_words.contains(c)) {
                List<Integer> indices = indexOfAll(c, lc_words);
                for (Integer i : indices) {
                    upperCaseConstants.add(words.get(i));
                }
            } else if (lc_words.contains(Pluralizer.pluralize(c))) {
                List<Integer> indices = indexOfAll(Pluralizer.pluralize(c), lc_words);
                for (Integer i : indices) {
                    upperCaseConstants.add(words.get(i));
                }
            } else {
                for (int idxstart = 0; idxstart < lc_words.size(); idxstart++) {
                    String w = lc_words.get(idxstart);
                    if (c.startsWith(w) && c.length() > w.length()) {
                        String concatword = w;
                        int idx = idxstart;
                        while (idx+1 < lc_words.size() && c.startsWith(concatword + lc_words.get(idx + 1))) {
                            concatword += lc_words.get(++idx);
                        }
                        if (c.equals(concatword)) {
                            concatword = words.get(idxstart);
                            for (int i = idxstart+1; i < idx+1; i++) {
                                concatword += "_" + words.get(i);
                            }

                            upperCaseConstants.add(concatword);
                        }
                    }
                }
            }
        }
        return upperCaseConstants;
    }

    private List<Integer> indexOfAll(String s, List<String> list) {
        final List<Integer> indexList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (s.equals(list.get(i))) {
                indexList.add(i);
            }
        }
        return indexList;
    }

    private Set<String> extractPredicateSymbols(Collection<String> answerset) {
        Set<String> strings = new HashSet<>();

        for (String s : answerset) {
            if (s.startsWith("-")) {
                strings.add(s.substring(1, s.indexOf("(")).trim());
            } else {
                strings.add(s.substring(0, s.indexOf("(")).trim());
            }
        }

        return strings;
    }

    private Set<String> extractConstantSymbols(Collection<String> answerset) {
        Set<String> strings = new HashSet<>();

        for (String s : answerset) {
            String sub = s.substring(s.indexOf("(")+1, s.indexOf(")"));
            String[] h = sub.split(",");
            for (int i = 0; i < h.length; i++) {
                if (h[i].length() > 1) {
                    strings.add(h[i].trim());
                }
            }
        }
        return strings;
    }

    public Set<String> getPREDS() {
        return PREDS;
    }

    public Set<String> getCONSTANTS() {
        return CONSTANTS;
    }

    public Set<String> getCNLPREDS() {
        return CNLPREDS;
    }

    public Set<String> getCNLCONSTANTS() {
        return CNLCONSTANTS;
    }
}
