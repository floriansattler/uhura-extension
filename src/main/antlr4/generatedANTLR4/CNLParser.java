// Generated from CNL.g4 by ANTLR 4.7.1
package generatedANTLR4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CNLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, DOESNOT=12, ART=13, VAR=14, TOBE=15, PH=16, MORELESS=17, 
		QUA=18, NUMB=19, NUMB_TEXT_ONE_TO_NINE=20, WORD=21, WS=22, NEWLINE=23, 
		C=24, PRED=25;
	public static final int
		RULE_text = 0, RULE_sentence = 1, RULE_sentence_types = 2, RULE_pattern1 = 3, 
		RULE_pattern2 = 4, RULE_pattern3 = 5, RULE_pattern4 = 6, RULE_pattern5 = 7, 
		RULE_pattern6 = 8, RULE_pattern7 = 9, RULE_ph = 10, RULE_artpred = 11, 
		RULE_artconst = 12, RULE_predvar = 13;
	public static final String[] ruleNames = {
		"text", "sentence", "sentence_types", "pattern1", "pattern2", "pattern3", 
		"pattern4", "pattern5", "pattern6", "pattern7", "ph", "artpred", "artconst", 
		"predvar"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'.'", "'If'", "'and'", "'or'", "'then'", "'Exclude that'", "'and that'", 
		"'or that'", "'There'", "'there'", "'normally'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"DOESNOT", "ART", "VAR", "TOBE", "PH", "MORELESS", "QUA", "NUMB", "NUMB_TEXT_ONE_TO_NINE", 
		"WORD", "WS", "NEWLINE", "C", "PRED"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "CNL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CNLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class TextContext extends ParserRuleContext {
		public List<SentenceContext> sentence() {
			return getRuleContexts(SentenceContext.class);
		}
		public SentenceContext sentence(int i) {
			return getRuleContext(SentenceContext.class,i);
		}
		public TextContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_text; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterText(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitText(this);
		}
	}

	public final TextContext text() throws RecognitionException {
		TextContext _localctx = new TextContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_text);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(28);
				sentence();
				}
				}
				setState(31); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__5) | (1L << T__8) | (1L << T__9) | (1L << ART) | (1L << VAR) | (1L << QUA) | (1L << C) | (1L << PRED))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SentenceContext extends ParserRuleContext {
		public List<Sentence_typesContext> sentence_types() {
			return getRuleContexts(Sentence_typesContext.class);
		}
		public Sentence_typesContext sentence_types(int i) {
			return getRuleContext(Sentence_typesContext.class,i);
		}
		public List<Pattern1Context> pattern1() {
			return getRuleContexts(Pattern1Context.class);
		}
		public Pattern1Context pattern1(int i) {
			return getRuleContext(Pattern1Context.class,i);
		}
		public SentenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterSentence(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitSentence(this);
		}
	}

	public final SentenceContext sentence() throws RecognitionException {
		SentenceContext _localctx = new SentenceContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sentence);
		int _la;
		try {
			setState(76);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(33);
				sentence_types();
				setState(34);
				match(T__0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(36);
				match(T__1);
				setState(37);
				sentence_types();
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2 || _la==T__3) {
					{
					{
					setState(38);
					_la = _input.LA(1);
					if ( !(_la==T__2 || _la==T__3) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(39);
					sentence_types();
					}
					}
					setState(44);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(45);
				match(T__4);
				setState(46);
				sentence_types();
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2 || _la==T__3) {
					{
					{
					setState(47);
					_la = _input.LA(1);
					if ( !(_la==T__2 || _la==T__3) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(48);
					sentence_types();
					}
					}
					setState(53);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(54);
				match(T__0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(56);
				match(T__5);
				setState(57);
				sentence_types();
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__6 || _la==T__7) {
					{
					{
					setState(58);
					_la = _input.LA(1);
					if ( !(_la==T__6 || _la==T__7) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(59);
					sentence_types();
					}
					}
					setState(64);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(65);
				match(T__0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(67);
				pattern1();
				setState(70); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(68);
					match(T__3);
					setState(69);
					pattern1();
					}
					}
					setState(72); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__3 );
				setState(74);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_typesContext extends ParserRuleContext {
		public Pattern1Context pattern1() {
			return getRuleContext(Pattern1Context.class,0);
		}
		public Pattern2Context pattern2() {
			return getRuleContext(Pattern2Context.class,0);
		}
		public Pattern3Context pattern3() {
			return getRuleContext(Pattern3Context.class,0);
		}
		public Pattern4Context pattern4() {
			return getRuleContext(Pattern4Context.class,0);
		}
		public Pattern5Context pattern5() {
			return getRuleContext(Pattern5Context.class,0);
		}
		public Pattern6Context pattern6() {
			return getRuleContext(Pattern6Context.class,0);
		}
		public Pattern7Context pattern7() {
			return getRuleContext(Pattern7Context.class,0);
		}
		public Sentence_typesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_types; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterSentence_types(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitSentence_types(this);
		}
	}

	public final Sentence_typesContext sentence_types() throws RecognitionException {
		Sentence_typesContext _localctx = new Sentence_typesContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sentence_types);
		try {
			setState(85);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(78);
				pattern1();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(79);
				pattern2();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(80);
				pattern3();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(81);
				pattern4();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(82);
				pattern5();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(83);
				pattern6();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(84);
				pattern7();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern1Context extends ParserRuleContext {
		public PhContext ph() {
			return getRuleContext(PhContext.class,0);
		}
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public TerminalNode DOESNOT() { return getToken(CNLParser.DOESNOT, 0); }
		public Pattern1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern1(this);
		}
	}

	public final Pattern1Context pattern1() throws RecognitionException {
		Pattern1Context _localctx = new Pattern1Context(_ctx, getState());
		enterRule(_localctx, 6, RULE_pattern1);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			ph();
			setState(89);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOESNOT || _la==TOBE) {
				{
				setState(88);
				_la = _input.LA(1);
				if ( !(_la==DOESNOT || _la==TOBE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(92);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(91);
				match(ART);
				}
			}

			setState(94);
			match(PRED);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern2Context extends ParserRuleContext {
		public List<PhContext> ph() {
			return getRuleContexts(PhContext.class);
		}
		public PhContext ph(int i) {
			return getRuleContext(PhContext.class,i);
		}
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public TerminalNode PH() { return getToken(CNLParser.PH, 0); }
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public TerminalNode DOESNOT() { return getToken(CNLParser.DOESNOT, 0); }
		public Pattern2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern2(this);
		}
	}

	public final Pattern2Context pattern2() throws RecognitionException {
		Pattern2Context _localctx = new Pattern2Context(_ctx, getState());
		enterRule(_localctx, 8, RULE_pattern2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			ph();
			setState(98);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOESNOT || _la==TOBE) {
				{
				setState(97);
				_la = _input.LA(1);
				if ( !(_la==DOESNOT || _la==TOBE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(100);
				match(ART);
				}
			}

			setState(103);
			match(PRED);
			setState(105);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PH) {
				{
				setState(104);
				match(PH);
				}
			}

			setState(107);
			ph();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern3Context extends ParserRuleContext {
		public PhContext ph() {
			return getRuleContext(PhContext.class,0);
		}
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public ArtpredContext artpred() {
			return getRuleContext(ArtpredContext.class,0);
		}
		public ArtconstContext artconst() {
			return getRuleContext(ArtconstContext.class,0);
		}
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public TerminalNode PH() { return getToken(CNLParser.PH, 0); }
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public TerminalNode DOESNOT() { return getToken(CNLParser.DOESNOT, 0); }
		public Pattern3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern3; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern3(this);
		}
	}

	public final Pattern3Context pattern3() throws RecognitionException {
		Pattern3Context _localctx = new Pattern3Context(_ctx, getState());
		enterRule(_localctx, 10, RULE_pattern3);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			ph();
			setState(111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOESNOT || _la==TOBE) {
				{
				setState(110);
				_la = _input.LA(1);
				if ( !(_la==DOESNOT || _la==TOBE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(114);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(113);
				match(ART);
				}
			}

			setState(116);
			match(PRED);
			setState(117);
			artpred();
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PH) {
				{
				setState(118);
				match(PH);
				}
			}

			setState(121);
			artconst();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern4Context extends ParserRuleContext {
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode VAR() { return getToken(CNLParser.VAR, 0); }
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public Pattern4Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern4; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern4(this);
		}
	}

	public final Pattern4Context pattern4() throws RecognitionException {
		Pattern4Context _localctx = new Pattern4Context(_ctx, getState());
		enterRule(_localctx, 12, RULE_pattern4);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			_la = _input.LA(1);
			if ( !(_la==T__8 || _la==T__9) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(124);
			match(TOBE);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(125);
				match(ART);
				}
			}

			setState(128);
			match(PRED);
			setState(129);
			match(VAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern5Context extends ParserRuleContext {
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode MORELESS() { return getToken(CNLParser.MORELESS, 0); }
		public TerminalNode NUMB() { return getToken(CNLParser.NUMB, 0); }
		public List<PredvarContext> predvar() {
			return getRuleContexts(PredvarContext.class);
		}
		public PredvarContext predvar(int i) {
			return getRuleContext(PredvarContext.class,i);
		}
		public TerminalNode C() { return getToken(CNLParser.C, 0); }
		public Pattern5Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern5; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern5(this);
		}
	}

	public final Pattern5Context pattern5() throws RecognitionException {
		Pattern5Context _localctx = new Pattern5Context(_ctx, getState());
		enterRule(_localctx, 14, RULE_pattern5);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(133);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case C:
				{
				setState(131);
				match(C);
				}
				break;
			case PRED:
				{
				setState(132);
				predvar();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(135);
			match(PRED);
			setState(136);
			match(MORELESS);
			setState(137);
			match(NUMB);
			setState(138);
			predvar();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern6Context extends ParserRuleContext {
		public List<TerminalNode> PRED() { return getTokens(CNLParser.PRED); }
		public TerminalNode PRED(int i) {
			return getToken(CNLParser.PRED, i);
		}
		public TerminalNode C() { return getToken(CNLParser.C, 0); }
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public Pattern6Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern6; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern6(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern6(this);
		}
	}

	public final Pattern6Context pattern6() throws RecognitionException {
		Pattern6Context _localctx = new Pattern6Context(_ctx, getState());
		enterRule(_localctx, 16, RULE_pattern6);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(PRED);
			setState(141);
			match(T__10);
			setState(143);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TOBE) {
				{
				setState(142);
				match(TOBE);
				}
			}

			setState(148);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(145);
				match(PRED);
				}
				break;
			case 2:
				{
				setState(146);
				match(PRED);
				setState(147);
				match(C);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pattern7Context extends ParserRuleContext {
		public TerminalNode QUA() { return getToken(CNLParser.QUA, 0); }
		public List<TerminalNode> PRED() { return getTokens(CNLParser.PRED); }
		public TerminalNode PRED(int i) {
			return getToken(CNLParser.PRED, i);
		}
		public TerminalNode TOBE() { return getToken(CNLParser.TOBE, 0); }
		public Pattern7Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern7; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPattern7(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPattern7(this);
		}
	}

	public final Pattern7Context pattern7() throws RecognitionException {
		Pattern7Context _localctx = new Pattern7Context(_ctx, getState());
		enterRule(_localctx, 18, RULE_pattern7);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			match(QUA);
			setState(151);
			match(PRED);
			setState(152);
			match(TOBE);
			setState(153);
			match(PRED);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PhContext extends ParserRuleContext {
		public TerminalNode C() { return getToken(CNLParser.C, 0); }
		public TerminalNode VAR() { return getToken(CNLParser.VAR, 0); }
		public PredvarContext predvar() {
			return getRuleContext(PredvarContext.class,0);
		}
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public PhContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ph; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPh(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPh(this);
		}
	}

	public final PhContext ph() throws RecognitionException {
		PhContext _localctx = new PhContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_ph);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(155);
				match(ART);
				}
			}

			setState(161);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case C:
				{
				setState(158);
				match(C);
				}
				break;
			case VAR:
				{
				setState(159);
				match(VAR);
				}
				break;
			case PRED:
				{
				setState(160);
				predvar();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArtpredContext extends ParserRuleContext {
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public ArtpredContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_artpred; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterArtpred(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitArtpred(this);
		}
	}

	public final ArtpredContext artpred() throws RecognitionException {
		ArtpredContext _localctx = new ArtpredContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_artpred);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(163);
				match(ART);
				}
			}

			setState(166);
			match(PRED);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArtconstContext extends ParserRuleContext {
		public TerminalNode C() { return getToken(CNLParser.C, 0); }
		public TerminalNode ART() { return getToken(CNLParser.ART, 0); }
		public ArtconstContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_artconst; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterArtconst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitArtconst(this);
		}
	}

	public final ArtconstContext artconst() throws RecognitionException {
		ArtconstContext _localctx = new ArtconstContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_artconst);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ART) {
				{
				setState(168);
				match(ART);
				}
			}

			setState(171);
			match(C);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredvarContext extends ParserRuleContext {
		public TerminalNode PRED() { return getToken(CNLParser.PRED, 0); }
		public TerminalNode VAR() { return getToken(CNLParser.VAR, 0); }
		public PredvarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predvar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).enterPredvar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CNLListener ) ((CNLListener)listener).exitPredvar(this);
		}
	}

	public final PredvarContext predvar() throws RecognitionException {
		PredvarContext _localctx = new PredvarContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_predvar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(173);
			match(PRED);
			setState(174);
			match(VAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\33\u00b3\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\6\2 \n\2\r\2\16\2!\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\7\3+\n\3\f\3\16\3.\13\3\3\3\3\3\3\3\3\3\7\3\64"+
		"\n\3\f\3\16\3\67\13\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3?\n\3\f\3\16\3B\13\3"+
		"\3\3\3\3\3\3\3\3\3\3\6\3I\n\3\r\3\16\3J\3\3\3\3\5\3O\n\3\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\5\4X\n\4\3\5\3\5\5\5\\\n\5\3\5\5\5_\n\5\3\5\3\5\3\6\3\6"+
		"\5\6e\n\6\3\6\5\6h\n\6\3\6\3\6\5\6l\n\6\3\6\3\6\3\7\3\7\5\7r\n\7\3\7\5"+
		"\7u\n\7\3\7\3\7\3\7\5\7z\n\7\3\7\3\7\3\b\3\b\3\b\5\b\u0081\n\b\3\b\3\b"+
		"\3\b\3\t\3\t\5\t\u0088\n\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\5\n\u0092\n"+
		"\n\3\n\3\n\3\n\5\n\u0097\n\n\3\13\3\13\3\13\3\13\3\13\3\f\5\f\u009f\n"+
		"\f\3\f\3\f\3\f\5\f\u00a4\n\f\3\r\5\r\u00a7\n\r\3\r\3\r\3\16\5\16\u00ac"+
		"\n\16\3\16\3\16\3\17\3\17\3\17\3\17\2\2\20\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\2\6\3\2\5\6\3\2\t\n\4\2\16\16\21\21\3\2\13\f\2\u00c3\2\37\3"+
		"\2\2\2\4N\3\2\2\2\6W\3\2\2\2\bY\3\2\2\2\nb\3\2\2\2\fo\3\2\2\2\16}\3\2"+
		"\2\2\20\u0087\3\2\2\2\22\u008e\3\2\2\2\24\u0098\3\2\2\2\26\u009e\3\2\2"+
		"\2\30\u00a6\3\2\2\2\32\u00ab\3\2\2\2\34\u00af\3\2\2\2\36 \5\4\3\2\37\36"+
		"\3\2\2\2 !\3\2\2\2!\37\3\2\2\2!\"\3\2\2\2\"\3\3\2\2\2#$\5\6\4\2$%\7\3"+
		"\2\2%O\3\2\2\2&\'\7\4\2\2\',\5\6\4\2()\t\2\2\2)+\5\6\4\2*(\3\2\2\2+.\3"+
		"\2\2\2,*\3\2\2\2,-\3\2\2\2-/\3\2\2\2.,\3\2\2\2/\60\7\7\2\2\60\65\5\6\4"+
		"\2\61\62\t\2\2\2\62\64\5\6\4\2\63\61\3\2\2\2\64\67\3\2\2\2\65\63\3\2\2"+
		"\2\65\66\3\2\2\2\668\3\2\2\2\67\65\3\2\2\289\7\3\2\29O\3\2\2\2:;\7\b\2"+
		"\2;@\5\6\4\2<=\t\3\2\2=?\5\6\4\2><\3\2\2\2?B\3\2\2\2@>\3\2\2\2@A\3\2\2"+
		"\2AC\3\2\2\2B@\3\2\2\2CD\7\3\2\2DO\3\2\2\2EH\5\b\5\2FG\7\6\2\2GI\5\b\5"+
		"\2HF\3\2\2\2IJ\3\2\2\2JH\3\2\2\2JK\3\2\2\2KL\3\2\2\2LM\7\3\2\2MO\3\2\2"+
		"\2N#\3\2\2\2N&\3\2\2\2N:\3\2\2\2NE\3\2\2\2O\5\3\2\2\2PX\5\b\5\2QX\5\n"+
		"\6\2RX\5\f\7\2SX\5\16\b\2TX\5\20\t\2UX\5\22\n\2VX\5\24\13\2WP\3\2\2\2"+
		"WQ\3\2\2\2WR\3\2\2\2WS\3\2\2\2WT\3\2\2\2WU\3\2\2\2WV\3\2\2\2X\7\3\2\2"+
		"\2Y[\5\26\f\2Z\\\t\4\2\2[Z\3\2\2\2[\\\3\2\2\2\\^\3\2\2\2]_\7\17\2\2^]"+
		"\3\2\2\2^_\3\2\2\2_`\3\2\2\2`a\7\33\2\2a\t\3\2\2\2bd\5\26\f\2ce\t\4\2"+
		"\2dc\3\2\2\2de\3\2\2\2eg\3\2\2\2fh\7\17\2\2gf\3\2\2\2gh\3\2\2\2hi\3\2"+
		"\2\2ik\7\33\2\2jl\7\22\2\2kj\3\2\2\2kl\3\2\2\2lm\3\2\2\2mn\5\26\f\2n\13"+
		"\3\2\2\2oq\5\26\f\2pr\t\4\2\2qp\3\2\2\2qr\3\2\2\2rt\3\2\2\2su\7\17\2\2"+
		"ts\3\2\2\2tu\3\2\2\2uv\3\2\2\2vw\7\33\2\2wy\5\30\r\2xz\7\22\2\2yx\3\2"+
		"\2\2yz\3\2\2\2z{\3\2\2\2{|\5\32\16\2|\r\3\2\2\2}~\t\5\2\2~\u0080\7\21"+
		"\2\2\177\u0081\7\17\2\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0083\7\33\2\2\u0083\u0084\7\20\2\2\u0084\17\3\2\2\2\u0085"+
		"\u0088\7\32\2\2\u0086\u0088\5\34\17\2\u0087\u0085\3\2\2\2\u0087\u0086"+
		"\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008a\7\33\2\2\u008a\u008b\7\23\2\2"+
		"\u008b\u008c\7\25\2\2\u008c\u008d\5\34\17\2\u008d\21\3\2\2\2\u008e\u008f"+
		"\7\33\2\2\u008f\u0091\7\r\2\2\u0090\u0092\7\21\2\2\u0091\u0090\3\2\2\2"+
		"\u0091\u0092\3\2\2\2\u0092\u0096\3\2\2\2\u0093\u0097\7\33\2\2\u0094\u0095"+
		"\7\33\2\2\u0095\u0097\7\32\2\2\u0096\u0093\3\2\2\2\u0096\u0094\3\2\2\2"+
		"\u0097\23\3\2\2\2\u0098\u0099\7\24\2\2\u0099\u009a\7\33\2\2\u009a\u009b"+
		"\7\21\2\2\u009b\u009c\7\33\2\2\u009c\25\3\2\2\2\u009d\u009f\7\17\2\2\u009e"+
		"\u009d\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a3\3\2\2\2\u00a0\u00a4\7\32"+
		"\2\2\u00a1\u00a4\7\20\2\2\u00a2\u00a4\5\34\17\2\u00a3\u00a0\3\2\2\2\u00a3"+
		"\u00a1\3\2\2\2\u00a3\u00a2\3\2\2\2\u00a4\27\3\2\2\2\u00a5\u00a7\7\17\2"+
		"\2\u00a6\u00a5\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9"+
		"\7\33\2\2\u00a9\31\3\2\2\2\u00aa\u00ac\7\17\2\2\u00ab\u00aa\3\2\2\2\u00ab"+
		"\u00ac\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00ae\7\32\2\2\u00ae\33\3\2\2"+
		"\2\u00af\u00b0\7\33\2\2\u00b0\u00b1\7\20\2\2\u00b1\35\3\2\2\2\31!,\65"+
		"@JNW[^dgkqty\u0080\u0087\u0091\u0096\u009e\u00a3\u00a6\u00ab";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}