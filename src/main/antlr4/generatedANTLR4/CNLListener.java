// Generated from CNL.g4 by ANTLR 4.7.1
package generatedANTLR4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CNLParser}.
 */
public interface CNLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CNLParser#text}.
	 * @param ctx the parse tree
	 */
	void enterText(CNLParser.TextContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#text}.
	 * @param ctx the parse tree
	 */
	void exitText(CNLParser.TextContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#sentence}.
	 * @param ctx the parse tree
	 */
	void enterSentence(CNLParser.SentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#sentence}.
	 * @param ctx the parse tree
	 */
	void exitSentence(CNLParser.SentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#sentence_types}.
	 * @param ctx the parse tree
	 */
	void enterSentence_types(CNLParser.Sentence_typesContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#sentence_types}.
	 * @param ctx the parse tree
	 */
	void exitSentence_types(CNLParser.Sentence_typesContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern1}.
	 * @param ctx the parse tree
	 */
	void enterPattern1(CNLParser.Pattern1Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern1}.
	 * @param ctx the parse tree
	 */
	void exitPattern1(CNLParser.Pattern1Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern2}.
	 * @param ctx the parse tree
	 */
	void enterPattern2(CNLParser.Pattern2Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern2}.
	 * @param ctx the parse tree
	 */
	void exitPattern2(CNLParser.Pattern2Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern3}.
	 * @param ctx the parse tree
	 */
	void enterPattern3(CNLParser.Pattern3Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern3}.
	 * @param ctx the parse tree
	 */
	void exitPattern3(CNLParser.Pattern3Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern4}.
	 * @param ctx the parse tree
	 */
	void enterPattern4(CNLParser.Pattern4Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern4}.
	 * @param ctx the parse tree
	 */
	void exitPattern4(CNLParser.Pattern4Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern5}.
	 * @param ctx the parse tree
	 */
	void enterPattern5(CNLParser.Pattern5Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern5}.
	 * @param ctx the parse tree
	 */
	void exitPattern5(CNLParser.Pattern5Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern6}.
	 * @param ctx the parse tree
	 */
	void enterPattern6(CNLParser.Pattern6Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern6}.
	 * @param ctx the parse tree
	 */
	void exitPattern6(CNLParser.Pattern6Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#pattern7}.
	 * @param ctx the parse tree
	 */
	void enterPattern7(CNLParser.Pattern7Context ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#pattern7}.
	 * @param ctx the parse tree
	 */
	void exitPattern7(CNLParser.Pattern7Context ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#ph}.
	 * @param ctx the parse tree
	 */
	void enterPh(CNLParser.PhContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#ph}.
	 * @param ctx the parse tree
	 */
	void exitPh(CNLParser.PhContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#artpred}.
	 * @param ctx the parse tree
	 */
	void enterArtpred(CNLParser.ArtpredContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#artpred}.
	 * @param ctx the parse tree
	 */
	void exitArtpred(CNLParser.ArtpredContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#artconst}.
	 * @param ctx the parse tree
	 */
	void enterArtconst(CNLParser.ArtconstContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#artconst}.
	 * @param ctx the parse tree
	 */
	void exitArtconst(CNLParser.ArtconstContext ctx);
	/**
	 * Enter a parse tree produced by {@link CNLParser#predvar}.
	 * @param ctx the parse tree
	 */
	void enterPredvar(CNLParser.PredvarContext ctx);
	/**
	 * Exit a parse tree produced by {@link CNLParser#predvar}.
	 * @param ctx the parse tree
	 */
	void exitPredvar(CNLParser.PredvarContext ctx);
}