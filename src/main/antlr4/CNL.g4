grammar CNL;

tokens { C, PRED }

@lexer::header {
import java.util.*;
}

@lexer::members {

private Set<String> preds;
private Set<String> constants;

public CNLLexer(CharStream input, Set<String> constants, Set<String> preds) {
	this(input);
	this.preds = preds;
	this.constants = constants;
}
}

text
	: sentence+
	;

sentence
	: sentence_types '.'
	| 'If' sentence_types (('and' | 'or') sentence_types)* 'then' sentence_types (('and' | 'or') sentence_types)* '.'
	| 'Exclude that' sentence_types (('and that' | 'or that') sentence_types)* '.'
	| pattern1 ('or' pattern1)+ '.'
	;

sentence_types
	: pattern1
	| pattern2
	| pattern3
	| pattern4
	| pattern5
	| pattern6
	| pattern7
	;

pattern1
	: ph (TOBE | DOESNOT)? ART? PRED
	;

pattern2
	: ph (TOBE | DOESNOT)? ART? PRED PH? ph
	;

pattern3
	: ph (TOBE | DOESNOT)? ART? PRED artpred PH? artconst
	;

pattern4
	: ('There' | 'there') TOBE ART? PRED VAR
	;

pattern5
	: (C | predvar) PRED MORELESS NUMB predvar
	;

pattern6
	: PRED 'normally' TOBE? (PRED | PRED C)
	;

pattern7
	: QUA PRED TOBE PRED
	;

ph
	: ART? (C | VAR | predvar)
	;

artpred
	: ART? PRED
	;

artconst
	: ART ? C
	;

predvar
	: PRED VAR
	;

DOESNOT
	: 'does not'
	| 'doesn\'t'
	| 'does'
	;

ART
	: 'a'
	| 'A'
	| 'an'
	| 'An'
	| 'the'
	| 'The'
	;

VAR
	: [A-Z]
	;

TOBE
	: 'is'
	| 'are'
	| 'is not'
	| 'isn\'t'
	| 'are not'
	| 'aren\'t'
	;

PH
	: 'as'
	| 'of'
	| 'at'
	;

MORELESS
	: 'more than'
	| 'less than'
	;

QUA
	: 'all'
	| 'some'
	| 'no'
	| 'All'
	| 'Some'
	| 'No'
	;

NUMB
	: ('0'..'9')+
	| NUMB_TEXT_ONE_TO_NINE
	;

NUMB_TEXT_ONE_TO_NINE
	: ('O'|'o')'ne' 
	| ('T'|'t')'wo' 
	| ('T'|'t')'hree' 
	| ('F'|'f')'our' 
	| ('F'|'f')'ive' 
	| ('S'|'s')'ix' 
	| ('S'|'s')'even' 
	| ('E'|'e')'ight'
	| ('N'|'n')'ine'
	;

WORD
	: ('a'..'z' | 'A'..'Z' | '_' | '-' | '0'..'9')+
	{
		if(preds.contains(getText())) {
			setType(CNLParser.PRED);
		} else if(constants.contains(getText())) {
			setType(CNLParser.C);
		}
	}
	;

WS
	: ( ' ' | '\t' )+ -> skip
	;

NEWLINE
	: ('\r'? '\n' | '\r') -> skip
	;

